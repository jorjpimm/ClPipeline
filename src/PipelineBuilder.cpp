#include "pipeline/PipelineBuilder.h"

namespace pipeline
{

PipelineBuilder::PipelineCall::PipelineCall(
  const Computation *function,
  std::vector<ObjectLocation> arguments)
: m_arguments(std::move(arguments))
, m_function(function)
{
}
PipelineBuilder::PipelineBuilder()
: m_next_id(0)
{
}

std::shared_ptr<PipelineBuilder::PipelineCall> PipelineBuilder::add_call(
  const Computation &computation,
  const std::vector<ObjectLocation> &inputs,
  std::vector<ObjectLocation> &outputs)
{
	if (inputs.size() != computation.inputs().size())
	{
		throw std::runtime_error("Invalid argument count passed to add_call");
	}

	for (std::size_t i = 0; i < inputs.size(); ++i)
	{
		if (inputs[i].type() != computation.inputs()[i])
		{
			throw std::runtime_error("Invalid argument passed to add_call");
		}
	}
  
  auto strat = ObjectLocation::AllocationStrategy::PerStream;
  switch(computation.strategy())
  {
  case Computation::EvaluationStrategy::PerStream:
    strat = ObjectLocation::AllocationStrategy::PerStream;
    break;
    case Computation::EvaluationStrategy::Single:
    strat = ObjectLocation::AllocationStrategy::Single;
    break;
  }

  outputs.clear();
  std::vector<ObjectLocation> arguments = inputs;
  for (auto output : computation.outputs())
  {
    if (computation.strategy() == Computation::EvaluationStrategy::PerStream &&
        output.strategy == ObjectLocation::AllocationStrategy::Single)
    {
      throw std::runtime_error("Outputting single object from a per stream computation is not allowed");
    }
    ObjectLocation obj{ make_next_id(), output.type, output.strategy };
    arguments.push_back(obj);
    outputs.push_back(obj);
		m_all_objects.push_back(obj);
  }
  auto ptr = std::make_shared<PipelineCall>(&computation, arguments);
	m_calls.push_back(ptr);
  return ptr;
}

ObjectLocation PipelineBuilder::add_argument(
  const Type *type,
  ObjectLocation::AllocationStrategy alloc_strat)
{
  ObjectLocation obj{ make_next_id(), type, alloc_strat };
	m_arguments.push_back(obj);
	m_all_objects.push_back(obj);
  return obj;
}

ObjectLocation PipelineBuilder::add_output(ObjectLocation output)
{
  m_outputs.push_back(output);
	return output;
}

ObjectLocation::Id PipelineBuilder::make_next_id()
{
  if (m_next_id == std::numeric_limits<decltype(m_next_id)>::max())
  {
    throw std::runtime_error("Too many objects in pipeline");
  }

  return m_next_id++;
}

}
