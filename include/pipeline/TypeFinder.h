#pragma once

#include "pipeline/Type.h"

namespace pipeline
{

template <typename T> class TypeFinder;

/// Define a new type which can be used to reference the type across an application
#define PIPELINE_DEFINE_TYPE(T) \
  namespace pipeline { template <> class TypeFinder<T> { public: \
    static const Type *get(); }; }

/// Implement a type (place in single source file) to create static type data
#define PIPELINE_IMPLEMENT_TYPE(T) \
  namespace pipeline { const Type *TypeFinder<T>::get() { \
    static auto t = Type::create<T>(#T); \
    return &t; } }

}
