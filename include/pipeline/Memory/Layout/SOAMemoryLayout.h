#pragma once

#include "pipeline/Memory/Memory.h"
#include "pipeline/Memory/Layout/LayoutHelper.h"
#include "pipeline/ObjectArray.h"
#include "pipeline/ObjectLocation.h"
#include "pipeline/Type.h"
#include "pipeline/TypedObject.h"
#include "pipeline/detail/cl.h"

#include <gsl/gsl-lite.h>

namespace pipeline
{

/// Memory layout type which lays out an array of pipeline objects for all streams next to each other
/// Then allocates a piece of memory for each pipeline object (for all streams at once)
///
/// Depending on workload and execution context, SOA could perform better in some situations.
template <typename MemoryType> class SOAMemoryLayout
{
public:
	/// Memory type returned by allocate calls - an
	/// array of allocations, one for each pipeline object.
	struct Memory
	{
		Memory(SOAMemoryLayout *l)
		: owner(true)
		, layout(l)
		{
		}

		Memory(Memory &&oth)
		: objects(std::move(oth.objects))
		, owner(true)
		, layout(oth.layout)
		{
			oth.owner = false;
		}

		Memory &operator=(Memory &&oth)
		{
			objects = std::move(oth.objects);
			owner = true;
			layout = oth.layout;
			oth.owner = false;
			return *this;
		}

		~Memory()
		{
			layout->destroy(*this);
		}

		Memory &operator=(const Memory &) = delete;
		Memory(const Memory &) = delete;

		std::vector<MemoryType> objects;
		bool owner;
		SOAMemoryLayout *layout;
	};

  /// Create a new memory layout based on given memory [objects], with [stream_count] streams.
	SOAMemoryLayout(
		const gsl::span<const ObjectLocation> &objects,
		std::size_t streams)
	: m_objects(objects.begin(), objects.end())
  , m_stream_count(streams)
	{
		m_requirements.resize(objects.size());
		m_stream_requirements.resize(objects.size());
		for (auto &obj : objects)
		{
			m_stream_requirements[obj.id()] = obj.type()->memory_description();
      if (obj.is_per_stream())
      {
        m_requirements[obj.id()] = MemoryDescription::array_of_n(m_stream_requirements[obj.id()], m_stream_count);
      }
      else
      {
        m_requirements[obj.id()] = m_stream_requirements[obj.id()];
      }
		}
	}

  /// Wrapper type for data returned by get_object
  /// Ensures any non-local memory is mapped after
  /// the [get_object] call.
	struct ObjectArrayType : public MemoryType::MappedType, ObjectArray
	{
		ObjectArrayType(
			typename MemoryType::MappedType &&mapping,
			const Type *type,
			std::size_t stream_count,
      std::size_t count,
			std::size_t element_size,
      std::size_t stride)
		: MemoryType::MappedType(std::move(mapping))
		, ObjectArray(ObjectArray::initialise(
			type,
			MemoryReference::initialise(*this, element_size * count),
			stream_count,
      count,
			stride
		))
		{
		}
	};

  /// Get the object for [object] at [stream]. Requires the memory
  /// allocated using [create] to be passed back in.
	ObjectArrayType get_object_array(MemoryMapType type, const Memory &allocated, ObjectLocation object) const
	{
    auto &object_loc = m_objects[object.id()];
		auto per_stream = object_loc.is_per_stream();
		auto &obj = allocated.objects[object.id()];
		return ObjectArrayType{
			obj.map(type),
			object.type(),
      m_stream_count,
      per_stream ? m_stream_count : 1,
      m_stream_requirements[object.id()].size(),
      per_stream ? m_stream_requirements[object.id()].size() : 0,
		};
	}

#ifdef PIPELINE_ENABLE_OPENCL
  /// Utility for opencl queues to access memory
  /// in cl format for an entire pipeline object at once.
	cl::Memory &get_object_cl_array(Memory &allocated, ObjectLocation object) const
	{
		auto &obj = allocated.objects[object.id()];
		return obj.cl();
	}
#endif

  /// Create memory for the objects using allocator alloc.
  /// This function expects [alloc] to provide a method [allocate]
  /// to allocate given memory requirements.
	template <typename Allocator> Memory create(Allocator &alloc)
	{
		Memory mem_out{ this };
		mem_out.objects.resize(m_requirements.size());
		for (std::size_t i = 0; i < mem_out.objects.size(); ++i)
		{
			mem_out.objects[i] = alloc.allocate(m_requirements[i]);
		}
    initialise_objects(*this, mem_out, m_objects);
		return mem_out;
	}

	/// Destroy the passed memory. This method is called
	/// automatically by the destructor of memory
	void destroy(Memory &mem)
	{
		if (!mem.owner)
		{
			return;
		}

		destroy_objects(*this, mem, m_objects);
		mem.owner = false;
	}


private:
	std::vector<MemoryDescription> m_stream_requirements;
	std::vector<MemoryDescription> m_requirements;
	std::vector<ObjectLocation> m_objects;
	std::size_t m_stream_count;
};
}
