#pragma once

#include "pipeline/Memory/Memory.h"
#include "pipeline/Memory/Layout/LayoutHelper.h"
#include "pipeline/ObjectArray.h"
#include "pipeline/ObjectLocation.h"
#include "pipeline/Type.h"
#include "pipeline/TypedObject.h"

#include <gsl/gsl-lite.h>

namespace pipeline
{

/// Memory layout type which lays out pipeline objects for a stream next to each other
/// Then one large array of stream memory containing all individual objects.
///
/// Depending on workload and execution context, AOS could perform better in some situations.
/// \note Not supported on opencl platforms
template <typename MemoryType> class AOSMemoryLayout
{
public:
	struct Memory
	{
		Memory(AOSMemoryLayout *l, MemoryType &&m)
		: memory(std::move(m))
		, owner(true)
		, layout(l)
		{
		}

		Memory(Memory &&oth)
		: memory(std::move(oth.memory))
		, owner(true)
		, layout(oth.layout)
		{
			oth.owner = false;
		}

		Memory &operator=(Memory &&oth)
		{
			memory = std::move(oth.memory);
			owner = true;
			layout = oth.layout;
			oth.owner = false;
			return *this;
		}

		~Memory()
		{
			layout->destroy(*this);
		}

		Memory &operator=(const Memory &) = delete;
		Memory(const Memory &) = delete;

		MemoryType memory;
		bool owner;
		AOSMemoryLayout *layout;
	};

  /// Create a new memory layout based on given memory [objects], with [stream_count] streams.
	AOSMemoryLayout(
    const gsl::span<const ObjectLocation> &objects,
    std::size_t stream_count)
	: m_objects(objects.begin(), objects.end())
  , m_stream_count(stream_count)
  {
		m_offsets.resize(objects.size());

		// first loop adding all per stream objects in a large array
		for (auto &obj : objects)
    {
			if (obj.is_per_stream())
			{
      	auto reference = m_stream_requirements.add(obj.type()->memory_description());
      	m_offsets[obj.id()] = reference;
			}
    }

		m_all_requirements = MemoryDescription::array_of_n(m_stream_requirements, stream_count);

		// Loop adding singular objects to the end
		for (auto &obj : objects)
    {
			if (!obj.is_per_stream())
			{
      	auto reference = m_all_requirements.add(obj.type()->memory_description());
      	m_offsets[obj.id()] = reference;
			}
    }
  }

	/// Wrapper type for data returned by get_object
	/// Ensures any non-local memory is mapped after
	/// the [get_object] call.
	struct ObjectArrayType : public MemoryType::MappedType, ObjectArray
	{
		ObjectArrayType(
			typename MemoryType::MappedType &&mapping,
			const Type *type,
			std::size_t data_byte_count,
      MemoryDescription::MemoryOffset element_offset,
      std::size_t stream_count,
      std::size_t count,
			std::size_t stride)
		: MemoryType::MappedType(std::move(mapping))
		, ObjectArray(ObjectArray::initialise(
			type,
			MemoryReference::initialise(*this, data_byte_count) + element_offset,
      stream_count,
			count,
			stride
		))
		{
		}
	};

  /// Get the object for [object] at [stream]. Requires the memory
  /// allocated using [create] to be passed back in.
	ObjectArrayType get_object_array(MemoryMapType type, const Memory &allocated, ObjectLocation object) const
  {
		auto per_stream = m_objects[object.id()].is_per_stream();
		return ObjectArrayType{
			allocated.memory.map(type),
			object.type(),
			m_all_requirements.size(),
			m_offsets[object.id()],
			m_stream_count,
      per_stream ? m_stream_count : 1,
			per_stream ? m_stream_requirements.size() : 0
		};
  }

	/// Create memory for the objects using allocator alloc.
	/// This function expects [alloc] to provide a method [allocate]
	/// to allocate given memory requirements.
	template <typename Allocator> Memory create(Allocator &alloc)
	{
		auto mem = Memory{ this, alloc.allocate(m_all_requirements) };

		initialise_objects(*this, mem, m_objects);
		return mem;
	}

	/// Destroy the passed memory. This method is called
	/// automatically by the destructor of memory
	void destroy(Memory &mem)
	{
		if (!mem.owner)
		{
			return;
		}

		destroy_objects(*this, mem, m_objects);
		mem.owner = false;
	}

private:
  MemoryDescription m_stream_requirements;
  MemoryDescription m_all_requirements;

  std::vector<MemoryDescription::MemoryOffset> m_offsets;
	std::vector<ObjectLocation> m_objects;
	std::size_t m_stream_count;
};
}
