#pragma once
#include "pipeline/Memory/Memory.h"
#include "pipeline/ObjectLocation.h"
#include "pipeline/Type.h"

#include <vector>

namespace pipeline
{

template <typename Layout> void initialise_objects(
  Layout &lay,
  typename Layout::Memory &mem,
  const std::vector<pipeline::ObjectLocation> &objects
)
{
  for (std::size_t i = 0; i < objects.size(); ++i)
  {
    auto type = objects[i].type();
    if (type->is_trivial())
    {
      continue;
    }

    auto arr = lay.get_object_array(MemoryMapType::Write, mem, objects[i]);
    for (auto e : arr)
    {
      auto ref = pipeline::MemoryReference::initialise(e.get(), type->memory_description().size());
      type->emplace(ref);
    }
  }
}

template <typename Layout> void destroy_objects(
	Layout &lay,
	typename Layout::Memory &mem,
	const std::vector<pipeline::ObjectLocation> &objects
	)
{
	for (std::size_t i = 0; i < objects.size(); ++i)
	{
		auto type = objects[i].type();
		if (type->is_trivial())
		{
			continue;
		}

		auto arr = lay.get_object_array(MemoryMapType::Write, mem, objects[i]);
		for (auto e : arr)
		{
			auto ref = pipeline::MemoryReference::initialise(e.get(), type->memory_description().size());
			type->destroy(ref);
		}
	}
}

}
