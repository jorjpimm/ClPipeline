#pragma once

#include "pipeline/Memory/MemoryDescription.h"

#include <cassert>

namespace pipeline
{

/// Reference to a local allocation without knowledge of its data origin
class MemoryReference
{
public:
  /// Create a reference to a null memory
	MemoryReference()
	: m_memory(nullptr)
	{
	}

  /// Initialise a reference pointing to [memory], with size [size]
	template <typename PointerType> static MemoryReference initialise(
    const PointerType &memory,
    std::size_t size)
	{
		return MemoryReference{ (void *)memory, size };
	}
  
  
  /// Initialise a reference pointing to [memory], with size [size]
  /// Overload taking a reference allowing passing of non-copyable, but
  /// castable to void * types.
  template <typename PointerType> static MemoryReference initialise(
    PointerType &memory,
    std::size_t size)
  {
		return MemoryReference{ (void *)memory, size };
  }

  /// Initialise a sub reference to memory inside [mem].
  /// \param mem    The parent allocation
  /// \param offset The offset into the reference
  /// \param size   The size of the sub allocation
	static MemoryReference initialise_sub(
    const MemoryReference &mem,
    MemoryDescription::MemoryOffset offset,
    std::size_t size)
	{
    assert(offset.offset() + size <= mem.m_size);
		return MemoryReference{ reinterpret_cast<char *>(mem.m_memory) + offset.offset(), size };
	}

    /// Initialise an array element from a reference containing an array
    /// \param mem            The array allocation
    /// \param element        The description of each element
    /// \param element_index  The index of the element
	static MemoryReference initialise_array_element(
    const MemoryReference &mem,
    const MemoryDescription &element,
    std::size_t element_index)
	{
		return MemoryReference{
      initialise_sub(
        mem,
        MemoryDescription::MemoryOffset{ element.size() * element_index },
        element.size())
    };
	}
  
  MemoryReference &operator+=(MemoryDescription::MemoryOffset offset)
  {
    m_memory += offset.offset();
    m_size -= offset.offset();
    return *this;
  }
  
  MemoryReference operator+(MemoryDescription::MemoryOffset offset) const
  {
    return MemoryReference(
      m_memory + offset.offset(),
      m_size - offset.offset());
  }

  /// Get the memory address
	void *get()
	{
		return m_memory;
	}

  /// Get the memory address
	const void *get() const
	{
		return m_memory;
	}
  
  bool operator!=(const MemoryReference &r) const
  {
  return m_memory != r.m_memory;
  }
  
  bool operator==(const MemoryReference &r) const
  {
  return m_memory == r.m_memory;
  }

private:
	MemoryReference(void *memory, std::size_t size)
	: m_memory((char *)memory)
  , m_size(size)
	{
	}

	char *m_memory;
  std::size_t m_size;
};

}
