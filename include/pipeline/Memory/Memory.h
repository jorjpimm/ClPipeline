#pragma once

namespace pipeline
{

enum class MemoryMapType
{
  Read = 1,
  Write = 2,
  ReadWrite = Read | Write
};

}
