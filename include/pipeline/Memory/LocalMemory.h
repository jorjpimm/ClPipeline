#pragma once

#include "pipeline/Memory/Memory.h"
#include "pipeline/Type.h"

#include <cstdlib>
#include <iostream>

namespace pipeline
{

/// Create memory on the local CPU
class LocalMemory
{
public:
  /// Create empty memory container
	LocalMemory()
  : m_memory(nullptr)
	{
	}

  /// Create memory for given [type]
	static LocalMemory create(const Type *type)
	{
		return create(type->memory_description());
	}

  /// Create memory for given [description]
	static LocalMemory create(const MemoryDescription &description)
	{
		// todo alignment...
		auto mem = LocalMemory{ std::malloc(description.size()) };
    return mem;
	}

  /// Move a memory allocation
  LocalMemory(LocalMemory &&oth)
  {
		m_memory = oth.m_memory;
    oth.m_memory = nullptr;
  }

  /// Move a memory allocation
  LocalMemory &operator=(LocalMemory &&oth)
  {
		std::swap(m_memory, oth.m_memory);
		return *this;
  }

  LocalMemory(const LocalMemory &) = delete;
  LocalMemory &operator=(const LocalMemory &) = delete;

  /// Destroy a memory allocation, freeing if t
  ~LocalMemory()
  {
		std::free(m_memory);
		m_memory = nullptr;
  }

	/// Mapped memory wrapper which holds pointer to local
	/// memory (does very little in local case - see ClMemory).
	struct MappedType
	{
		void *mem;

		/// Cast the mapped memory to a local pointer
		operator void *()
		{
			return value();
		}

		/// Get the mapped memory as a local pointer
		void *value()
		{
			return mem;
		}
	};

	/// Map memory so its available for use in the calling function
  MappedType map(
		MemoryMapType,
    const MemoryDescription::MemoryOffset &offset = MemoryDescription::MemoryOffset(),
    std::size_t size = std::numeric_limits<std::size_t>::max()) const
	{
		return MappedType{ reinterpret_cast<char *>(m_memory) + offset.offset() };
	}

private:
	LocalMemory(void *memory)
	{
		m_memory = memory;
	}

	void *m_memory = nullptr;
};

}
