#pragma once

#include <algorithm>
#include <cstddef>
#include <type_traits>

namespace pipeline
{

/// A description of a type in memory, containing the size and
/// alignment of other types.
class MemoryDescription
{
public:
  /// An offset into another type, represented in bytes
	class MemoryOffset
	{
	public:
    /// An empty offset
		MemoryOffset()
		: m_offset(0)
		{
		}

    /// Specify an offset in bytes
		MemoryOffset(std::size_t off)
		: m_offset(off)
		{
		}

    /// Find the offset in bytes
		std::size_t offset() const { return m_offset; }

    /// Sum two offsets, finding the aggregated offset
		MemoryOffset operator+(MemoryOffset off) const
		{
			return { m_offset + off.m_offset };
		}
  
    MemoryOffset operator*(const std::size_t cnt) const
    {
      return { m_offset * cnt };
    }

    /// Check if two offsets are equal
    bool operator==(const MemoryOffset &off) const
    {
      return m_offset == off.m_offset;
    }

	private:
		std::size_t m_offset;
		friend class MemoryDescription;
		friend class MemoryReference;
	};

  /// Create a default empty description, with no size.
	MemoryDescription()
	: m_size(0)
	, m_alignment(4)
	{
	}

  /// Create a description for a given size and alignment
	MemoryDescription(std::size_t size, std::size_t alignment)
	: m_size(size)
	, m_alignment(alignment)
	{
	}

  /// Create a description for a type
	template <typename T> static MemoryDescription for_type()
	{
		return MemoryDescription(sizeof(T), std::alignment_of<T>::value);
	}

  /// Find a description for an array of [element] types
	static MemoryDescription array_of_n(MemoryDescription &element, std::size_t cnt)
	{
		element.align_size_to(element.alignment());

		MemoryDescription output;
		output.m_size = element.size() * cnt;
		output.m_alignment = element.m_alignment;
		return output;
	}

	/// Append a new type to the end of this type,
	/// aligning this type correctly for the aggregation
	MemoryOffset add(const MemoryDescription &desc)
	{
		align_size_to(desc.alignment());
		MemoryOffset offset{ m_size };
		m_size += desc.size();
		m_alignment = std::max(m_alignment, desc.alignment());
		return offset;
	}

	/// Align the size of this type to [bytes]
	void align_size_to(std::size_t bytes)
  {
    auto div = m_size % bytes;
    if (div != 0)
    {
      m_size += bytes - div;
    }
  }

	/// Get the size of this type
	std::size_t size() const { return m_size; }
	/// Get the alignemnt of this type in bytes
	std::size_t alignment() const { return m_alignment; }
  
  /// Check if two descriptions are equal
  bool operator==(const MemoryDescription &desc) const
  {
    return m_size == desc.m_size && m_alignment == desc.m_alignment;
  }
  
private:
	std::size_t m_size;
	std::size_t m_alignment;
};

}
