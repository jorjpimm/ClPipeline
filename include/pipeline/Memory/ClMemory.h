#pragma once

#ifdef PIPELINE_ENABLE_OPENCL

#include "pipeline/Memory/Memory.h"
#include "pipeline/Memory/MemoryDescription.h"
#include "pipeline/detail/cl.h"

namespace pipeline
{

/// Reference memory in an openCL device
class ClMemory
{
public:
	/// Create default memory, referencing no remote data
	ClMemory()
	{
	}

	/// Initialise cl memory referencing [cl_memory], with size [size].
	/// \param cl_memory 	cl memory to reference
	/// \param size				size of the allocation to reference
	/// \param map_queue	queue to schecule map operations on when required
  static ClMemory initialise(
		cl::Memory &&cl_memory,
		std::size_t size,
		cl::CommandQueue *map_queue)
	{
		return ClMemory{ std::move(cl_memory), size, map_queue };
	}

	/// Move cl memory
  ClMemory(ClMemory &&oth)
  : m_cl(std::move(oth.m_cl))
	, m_size(oth.m_size)
	, m_map_queue(oth.m_map_queue)
  {
  }

	/// Move cl memory
  ClMemory &operator=(ClMemory &&oth)
  {
    m_cl = std::move(oth.m_cl);
		m_size = oth.m_size;
		m_map_queue = oth.m_map_queue;
    return *this;
  }

  ClMemory(const ClMemory &) = delete;
  ClMemory &operator=(const ClMemory &) = delete;

  ~ClMemory()
  {
  }

	/// Mapped memory wrapper which releases mapping when it goes out of scope.
	class MappedMemory
	{
	public:
		MappedMemory(cl::MemoryMapping &&mapping)
		: m_mapping(std::move(mapping))
		{
		}

		/// Cast the mapped memory to a local pointer
		operator void *()
		{
			return value();
		}

		/// Get the mapped memory as a local pointer
    void *value()
    {
      return m_mapping.get();
    }

	private:
		cl::MemoryMapping m_mapping;
	};

	using MappedType = MappedMemory;
	/// Map this memory into local CPU memory
	/// \param offset 	The offset into the memory
	/// \param in_size	The size of the memory to map locally
  MappedType map(
		MemoryMapType map_type,
		const MemoryDescription::MemoryOffset &offset =
			MemoryDescription::MemoryOffset(),
		std::size_t in_size =
			std::numeric_limits<std::size_t>::max()) const
	{
    auto size = in_size;
    if (size == std::numeric_limits<std::size_t>::max())
    {
      size = m_size;
    }
		cl_map_flags map_flags = 0;
		if (int(map_type) & int(MemoryMapType::Read))
		{
			map_flags = map_flags | CL_MAP_READ;
		}
		if (int(map_type) & int(MemoryMapType::Write))
		{
			map_flags = map_flags | CL_MAP_WRITE;
		}

		return MappedMemory{ m_map_queue->map(m_cl, CL_TRUE, map_flags, offset.offset(), size) };
	}

	/// Get the cl memory as a reference to the cl type
	cl::Memory &cl() { return m_cl; };

private:
  ClMemory(cl::Memory &&memory, std::size_t size, cl::CommandQueue *map_queue)
  : m_cl(std::move(memory))
  , m_size(size)
	, m_map_queue(map_queue)
	{
  }

	cl::Memory m_cl;
  std::size_t m_size;
	cl::CommandQueue *m_map_queue;
};

}

#endif