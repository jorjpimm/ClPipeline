#pragma once

#ifdef PIPELINE_ENABLE_OPENCL

#include "pipeline/Computation/ClComputation.h"
#include "pipeline/PipelineBuilder.h"
#include "pipeline/Queue/Queue.h"
#include "pipeline/detail/cl.h"

#include <mutex>
#include <unordered_map>

namespace pipeline
{

/// A cl queue schedules work on a given cl device context
template <typename MemoryLayout> class ClQueue : public Queue
{
public:
	/// Create a new cl queue, running inside [context]
	/// \param context  The context to run commands inside
	/// \param finished A callback to make when each queue item is finished
	ClQueue(cl::Context &context, OnFinished finished = {})
	: m_queue(context.create_command_queue())
	, m_next_enqueue(0)
	, m_on_finished(finished)
	{
	}
  
  ~ClQueue()
  {
    // Wait for the queue to empty before
    // exiting - otherwise events may get lost.
    //
    // Different cl runtime have different modes when closing down cl contexts
    // some destroy all events without emitting, some emit them all, waiting here
    // smooths over the differences
    //
    while (true)
    {
      {
        std::lock_guard<std::mutex> l(m_waiting_events_mutex);
        if (m_waiting_events.empty())
        {
          break;
        }
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(5));
    }
  }

	/// Add a new item onto the queue, to be completed as soon as possible.
	EnqueueResult enqueue(
		const PipelineBuilder::PipelineCall &call,
		std::size_t stream_count,
		const MemoryLayout &layout,
		typename MemoryLayout::Memory &memory)
	{
		assert(call.function()->type() == Computation::ComputationType::CL);
		auto args = call.arguments();
		const ClComputation *comp = static_cast<const ClComputation*>(call.function());

		auto &kernel = comp->kernel();

		for (std::size_t i = 0; i < args.size(); ++i)
		{
			cl::Memory &mem = layout.get_object_cl_array(memory, args[i]);
			kernel.set_kernel_arg(std::uint32_t(i), mem);
		}
  
    auto inv_count = comp->strategy() == Computation::EvaluationStrategy::PerStream ?
      stream_count :
      1;
  
		auto result = m_next_enqueue++;
		if (!m_on_finished)
		{
			m_queue.execute(kernel, inv_count);
		}
		else
		{
			auto created_ev = m_queue.execute_with_event(kernel, inv_count);
			{
				std::lock_guard<std::mutex> l(m_waiting_events_mutex);
				m_waiting_events[result] = std::move(created_ev);
			}

			auto &ev = m_waiting_events[result];
			ev.set_completion_callback([this, result]()
			{
				m_on_finished(result);
				std::lock_guard<std::mutex> l(m_waiting_events_mutex);
        m_waiting_events.erase(result);
			});
		}

		return result;
	}

	/// Block for all queue items to be completed
	void block_for_finish()
	{
		m_queue.finish();
	}

	/// Accessor for the internal open cl command queue
	cl::CommandQueue &cl_queue() { return m_queue; }
	/// Accessor for the internal open cl command queue
	const cl::CommandQueue &cl_queue() const { return m_queue; }

private:
	cl::CommandQueue m_queue;
	EnqueueResult m_next_enqueue;
	OnFinished m_on_finished;

	std::mutex m_waiting_events_mutex;
	std::unordered_map<EnqueueResult, cl::Event> m_waiting_events;
};
}

#endif
