#pragma once

#include "pipeline/Computation/LocalComputation.h"
#include "pipeline/Memory/Memory.h"
#include "pipeline/Queue/Queue.h"
#include "pipeline/PipelineBuilder.h"

#include <atomic>
#include <deque>
#include <mutex>
#include <thread>

namespace pipeline
{

/// A local queue schedules work on local cpu threads (in the current process).
template <typename MemoryLayout> class LocalQueue : public Queue
{
	static const std::size_t MaxParameters = 16;
public:
  /// Create a new local queue
  /// \param finished A callback to make when each queue item is finished
	LocalQueue(OnFinished finished = {})
	: m_on_finished(finished)
	, m_next_enqueue(0)
	, m_working_count(0)
	{
		m_exit = false;
		m_worker = std::thread([&]() { run_thread(); });
	}

	~LocalQueue()
	{
		block_for_finish();
		m_exit = true;
		m_worker.join();
	}

  /// Add a new item onto the queue, to be completed as soon as possible.
	EnqueueResult enqueue(
		const PipelineBuilder::PipelineCall &call,
		std::size_t stream_count,
		const MemoryLayout &layout,
		typename MemoryLayout::Memory &memory)
	{
		assert(call.function()->type() == Computation::ComputationType::Local);
		auto item = WorkItem{
			&call,
			stream_count,
			&layout,
			&memory,
			m_next_enqueue++
		};

		{
			std::lock_guard<std::mutex> l(m_queued_lock);
			m_queued.push_back(item);
		}

		return item.result;
	}

  /// Block for all queue items to be completed
	void block_for_finish()
	{
		while (true)
		{
			{
				std::lock_guard<std::mutex> l(m_queued_lock);
				if (m_queued.empty() && m_working_count == 0)
				{
					return;
				}
			}

			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
	}

private:
  /// An item of work to be completed by the queue
  struct WorkItem
  {
		const PipelineBuilder::PipelineCall *call;
		std::size_t stream_count;
		const MemoryLayout *layout;
		typename MemoryLayout::Memory *memory;
		EnqueueResult result;
  };
  
  /// Run an entire work item
  void run(const WorkItem &item)
  {
		auto args = item.call->arguments();

		m_parameters.reserve(args.size());
		m_parameter_arrays.reserve(args.size());
    for (std::size_t i = 0; i < args.size(); ++i)
    {
      m_parameters.emplace_back(item.layout->get_object_array(MemoryMapType::ReadWrite, *item.memory, args[i]));
			m_parameter_arrays.emplace_back(&m_parameters.back());
    }
  
		auto func = static_cast<const LocalComputation*>(item.call->function());
    pipeline::LocalComputation::MultiStreamArguments inv_args(m_parameter_arrays);
  
    auto inv_count = func->strategy() == Computation::EvaluationStrategy::PerStream ?
      item.stream_count :
      1;
		for (std::size_t i = 0; i < inv_count; ++i)
    {
      func->call(i, inv_args);
    }

		m_parameter_arrays.clear();
		m_parameters.clear();
  
		if (m_on_finished)
    {
      m_on_finished(item.result);
    }
  }
  
  /// Entrypoint for the work thread
  void run_thread()
  {
		bool wait = false;
		while (!m_exit)
    {
      if (wait)
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
      }
      
      ++m_working_count;
      auto reset = gsl::finally([&]() { --m_working_count; });
      
      WorkItem to_process;
      {
        std::lock_guard<std::mutex> l(m_queued_lock);
        if (!m_queued.size())
        {
          wait = true;
          continue;
        }
        
        to_process = m_queued.front();
        m_queued.pop_front();
      }
      
      run(to_process);
    }
  }
  
	std::thread m_worker;
	OnFinished m_on_finished;
	EnqueueResult m_next_enqueue;

	std::mutex m_queued_lock;
	std::deque<WorkItem> m_queued;
	std::atomic<std::size_t> m_working_count;

	std::vector<typename MemoryLayout::ObjectArrayType> m_parameters;
	std::vector<ObjectArray *> m_parameter_arrays;

	std::atomic<bool> m_exit;
};

}
