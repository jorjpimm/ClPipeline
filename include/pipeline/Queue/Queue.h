#pragma once

#include <cstddef>
#include <functional>

namespace pipeline
{

/// Base class for all queues, contains basic queuing types.
/// 
/// A queue is a list of operations which should be completed in order
/// commands should be completed sequentially, as quickly as a queue type
/// can schedule.
class Queue
{
public:
	using EnqueueResult = std::size_t;
	using OnFinished = std::function<void(EnqueueResult)>;
};

}
