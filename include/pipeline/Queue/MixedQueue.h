#pragma once

#ifdef PIPELINE_ENABLE_OPENCL

#include "pipeline/PipelineBuilder.h"
#include "pipeline/Queue/Queue.h"
#include "pipeline/Queue/LocalQueue.h"
#include "pipeline/Queue/ClQueue.h"
#include "pipeline/detail/cl.h"

namespace std
{
template<typename A, typename B> struct hash<std::pair<A, B>>
{
	typedef std::pair<A, B> argument_type;
	typedef std::size_t result_type;
	result_type operator()(argument_type const& s) const
	{
		result_type const h1(std::hash<A>()(s.first));
		result_type const h2(std::hash<B>()(s.second));
		return h1 ^ h2;
	}
};
  
template<> struct hash<pipeline::Computation::ComputationType>
{
	typedef pipeline::Computation::ComputationType argument_type;
  typedef std::underlying_type<argument_type>::type underlying_type;
	typedef std::size_t result_type;
	result_type operator()(argument_type const& s) const
	{
		result_type const h1(std::hash<underlying_type>()((underlying_type)s));
		return h1;
	}
};
}

namespace pipeline
{

/// A mixed queue schedules work on both local cpu threads and opencl devices.
/// Barriers are added when data needs to be brought between the different queues
/// in order to keep both queues synchronised.
template <typename MemoryLayout> class MixedQueue : public Queue
{
public:
	/// Create a new mixed queue, running cl tasks inside [context]
	/// \param context  The context to run commands inside
	/// \param finished A callback to make when each queue item is finished
	MixedQueue(cl::Context &context, OnFinished finished = {})
  : m_local_queue([&](EnqueueResult finished) { task_finished(finished, Computation::ComputationType::Local); })
  , m_cl_queue(context, [&](EnqueueResult finished) { task_finished(finished, Computation::ComputationType::CL); })
  , m_on_finished(finished)
	, m_next_enqueue(0)
	{
		m_last.first = Computation::ComputationType::Unknown;
	}

  /// Add a new item onto the queue, to be completed as soon as possible.
	EnqueueResult enqueue(
		const PipelineBuilder::PipelineCall &call,
		std::size_t stream_count,
		const MemoryLayout &layout,
		typename MemoryLayout::Memory &memory)
	{
		auto type = call.function()->type();
		if (m_last.first == Computation::ComputationType::Unknown)
		{
			m_last.first = type;
		}

		auto id = m_next_enqueue++;
		if (type == m_last.first && m_to_enqueue.size() == 0)
		{
			enqueue_now(call, stream_count, layout, memory, id);
		}
		else
		{
			add_enqueue_pending_finish(m_last_enqueue, call, stream_count, layout, memory, id);
		}

		m_last_enqueue = id;
		return id;
	}

  /// Block for all enqueued tasks to be finished.
	void block_for_finish()
	{
		while(m_to_enqueue.size())
		{
			m_cl_queue.block_for_finish();
			m_local_queue.block_for_finish();
		}
	}

	/// Accessor for the internal local queue
	LocalQueue<MemoryLayout> &local_queue() { return m_local_queue; }
	/// Accessor for the internal local queue
	const LocalQueue<MemoryLayout> &local_queue() const { return m_local_queue; }

	/// Accessor for the internal cl queue
	ClQueue<MemoryLayout> &cl_queue() { return m_cl_queue; }
	/// Accessor for the internal cl queue
	const ClQueue<MemoryLayout> &cl_queue() const { return m_cl_queue; }

private:
	using EnqueuedChild = std::pair<Computation::ComputationType, EnqueueResult>;
	EnqueuedChild m_last;

	struct ToEnqueue
	{
		EnqueueResult waiting_on;
		const PipelineBuilder::PipelineCall *call;
		std::size_t stream_count;
		const MemoryLayout *layout;
		typename MemoryLayout::Memory *memory;
		EnqueueResult id;
	};

  /// Invoked by sub queues when a task is finished (via lambdas in ctor)
	void task_finished(EnqueueResult finished, Computation::ComputationType type)
	{
		auto to_find = std::make_pair(type, finished);
		auto my_result_it = m_enqueued_pairs.find(to_find);
		assert(my_result_it != m_enqueued_pairs.end());

		auto my_finished_result = my_result_it->second;
		if (m_on_finished)
		{
			m_on_finished(my_finished_result);
		}
		m_enqueued_pairs.erase(my_result_it);

		if (m_to_enqueue.empty())
		{
			return;
		}

		auto &poss_next = m_to_enqueue.front();
		if (poss_next.waiting_on == my_finished_result)
		{
			auto next_type = poss_next.call->function()->type();
			while (m_to_enqueue.size())
			{
				auto &next = m_to_enqueue.front();
				if (next.call->function()->type() != next_type)
				{
					break;
				}

				enqueue_now(*next.call, next.stream_count, *next.layout, *next.memory, next.id);

				m_to_enqueue.pop_front();
			}
		}
	}

  /// Enqueue the given task on the correct queue now - no
  /// waiting for previous tasks on any queues.
	void enqueue_now(
		const PipelineBuilder::PipelineCall &call,
		std::size_t stream_count,
		const MemoryLayout &layout,
		typename MemoryLayout::Memory &memory,
		EnqueueResult id)
	{
		auto type = call.function()->type();
		if (type == Computation::ComputationType::Local)
		{
			m_last.second = m_local_queue.enqueue(call, stream_count, layout, memory);
		}
		else if (type == Computation::ComputationType::CL)
		{
			m_last.second = m_cl_queue.enqueue(call, stream_count, layout, memory);
		}
		else
		{
			assert(0 && "Invalid type");
		}
		m_last.first = type;
		m_enqueued_pairs[m_last] = id;
	}

  /// Add a pending enqueue, waiting for [waiting_on] to be completed.
	void add_enqueue_pending_finish(
		EnqueueResult waiting_on,
		const PipelineBuilder::PipelineCall &call,
		std::size_t stream_count,
		const MemoryLayout &layout,
		typename MemoryLayout::Memory &memory,
		EnqueueResult id)
	{
		m_to_enqueue.emplace_back(ToEnqueue{
			waiting_on,
			&call,
			stream_count,
			&layout,
			&memory,
			id
		});
		m_last = std::make_pair(call.function()->type(), id);
	}

	LocalQueue<MemoryLayout> m_local_queue;
	ClQueue<MemoryLayout> m_cl_queue;
	OnFinished m_on_finished;

	std::deque<ToEnqueue> m_to_enqueue;
	std::unordered_map<EnqueuedChild, EnqueueResult> m_enqueued_pairs;

	EnqueueResult m_last_enqueue;
	EnqueueResult m_next_enqueue;
};

}

#endif
