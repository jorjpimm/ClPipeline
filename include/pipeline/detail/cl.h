#pragma once

#ifdef PIPELINE_ENABLE_OPENCL
#include <iostream>
#include <atomic>
#include <functional>
#include <thread>
#include <vector>

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS
#if !defined(__APPLE__)
# include <CL/cl.h>
#else
# include <OpenCL/opencl.h>
#endif

namespace cl
{
class Device;

template <typename Derived, typename _IdType> class ClObject
{
public:
	using IdType = _IdType;

	ClObject(IdType id=nullid())
	: m_id(id)
	{
	}

	~ClObject()
	{
		reset();
	}

	ClObject(ClObject &&oth)
	{
		m_id = oth.m_id;
		oth.m_id = nullid();
	}

	ClObject &operator=(ClObject &&oth)
	{
		m_id = oth.m_id;
		oth.m_id = nullid();
    return *this;
	}

	ClObject(const ClObject &) = delete;
	ClObject &operator=(const ClObject &) = delete;

	Derived *derived()
	{
		return static_cast<Derived *>(this);
	}

	const Derived *derived() const
	{
		return static_cast<const Derived *>(this);
	}

	void assign(IdType id)
	{
		reset();
		m_id = id;
	}

	void reset()
	{
		auto d = derived();
		if (d->valid())
		{
			d->release();
			m_id = nullid();
		}
	}

	bool valid() const
	{
		return m_id != nullid();
	}

	static IdType nullid()
	{
		return 0;
	}

protected:
	IdType m_id;
};

#define CL_OBJECT_CTORS(cls) \
  cls(cls::IdType id) { assign(id); }


class Memory : public ClObject<Memory, cl_mem>
{
public:
	Memory(IdType id = nullid())
	: ClObject(id)
	{
	}

  void release()
  {
    clReleaseMemObject(m_id);
  }

private:
  friend class CommandQueue;
  friend class Context;
  friend class Kernel;
};

class Kernel : public ClObject<Kernel, cl_kernel>
{
public:
  Kernel(cl_device_id device, IdType id)
  : ClObject(id)
	, m_device(device)
  {
  }

	void release()
	{
    clReleaseKernel(m_id);
  }

  void set_kernel_arg(std::uint32_t i, const Memory &m)
  {
    int err = clSetKernelArg(m_id, i, sizeof(cl_mem), &m.m_id);
    if (err != CL_SUCCESS)
    {
      throw std::runtime_error("Error: Failed to set kernel arguments!");
    }
  }

  template <typename T> void set_kernel_arg(std::uint32_t i, const T &t)
  {
    int err = clSetKernelArg(m_id, i, sizeof(T), &t);
    if (err != CL_SUCCESS)
    {
      throw std::runtime_error("Error: Failed to set kernel arguments!");
    }
  }

  /// Get the maximum work group size for executing the kernel on the device
  std::size_t work_group_size() const
  {
    std::size_t local;
    int err = clGetKernelWorkGroupInfo(m_id, m_device, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL);
    if (err != CL_SUCCESS)
    {
      throw std::runtime_error("Error: Failed to retrieve kernel work group info!");
    }
    return local;
  }

private:
  cl_device_id m_device;
  friend class CommandQueue;
  friend class Program;
};

class MemoryMapping : public ClObject<MemoryMapping, cl_mem>
{
public:
	MemoryMapping(cl_mem mem, void *memory, cl_command_queue queue)
	: ClObject(mem)
	, m_memory(memory)
	, m_queue(queue)
	{
	}

	void release()
	{
		clEnqueueUnmapMemObject(m_queue, m_id, m_memory, 0, NULL, NULL);
	}

	template <typename T> T *as()
	{
		return static_cast<T*>(m_memory);
	}

	void *get()
	{
		return m_memory;
	}

private:
	void *m_memory;
	cl_command_queue m_queue;
};

class Event : public ClObject<Event, cl_event>
{
public:
  Event()
  {
  }

  ~Event()
  {
    if (m_id)
    {
			// Blocking for unfinshed events should be unlikely - 
			// The point is to create them and wait for them to fire.
			//
			// If this is an issue, maybe a condition variable would be
			// better (or tuning the timer?), but it may make Event quite
			// heavy weight...
      clWaitForEvents(1, &m_id);
			while (!m_finished)
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(50));
			}
    }
  }

  Event(Event &&oth)
  : ClObject(std::move(oth))
  {
    if (m_callback)
    {
      throw std::runtime_error("cannot move initialised events");
    }
  }

  Event &operator=(Event &&oth)
  {
    ((ClObject &)*this) = std::move(oth);
    if (m_callback)
    {
      throw std::runtime_error("cannot move initialised events");
    }

    return *this;
  }

  void release()
  {
		clReleaseEvent(m_id);
  }

  template <typename T> void set_completion_callback(T t)
  {
    void (*cb)(cl_event, cl_int, void *);
    cb = [](cl_event i_ev, cl_int st, void *ud)
    {
      Event *ev = (Event *)ud;
      ev->m_finished = true;
      ev->m_callback();
      ev->m_id = nullid();
    };

    m_callback = t;
    clSetEventCallback(m_id, CL_COMPLETE, cb, this);
  }

private:
  friend class CommandQueue;
  std::function<void()> m_callback;
	std::atomic<bool> m_finished;
};

class CommandQueue : public ClObject<CommandQueue, cl_command_queue>
{
public:
  CommandQueue(IdType id)
	: ClObject(id)
  {
  }

  void release()
  {
    clReleaseCommandQueue(m_id);
  }

  // Write data set into the input array in device memory
  void write(Memory &memory, cl_bool blocking, std::size_t offset, std::size_t size, const void *data)
  {
  int err = clEnqueueWriteBuffer(m_id, memory.m_id, blocking, offset, size, data, 0, NULL, NULL);
  if (err != CL_SUCCESS)
    {
    throw std::runtime_error("Error: Failed to write to source array!");
    }
  }

	// Read back the results from the device to verify the output
	void read(Memory &memory, cl_bool blocking, std::size_t offset, std::size_t size, void *results)
	{
		int err = clEnqueueReadBuffer(m_id, memory.m_id, blocking, offset, size, results, 0, NULL, NULL);
		if (err != CL_SUCCESS)
		{
			throw std::runtime_error("Error: Failed to read the source array!");
		}
	}

	// Map memory on the device into local memory space
	MemoryMapping map(const Memory &memory, cl_bool blocking, cl_map_flags flags, std::size_t offset, std::size_t size)
	{
		int err = 0;
		void *mem = clEnqueueMapBuffer(m_id, memory.m_id, blocking, flags, offset, size, 0, NULL, NULL, &err);
		if (err != CL_SUCCESS)
		{
			throw std::runtime_error("Error: Failed to map the source array!");
		}
		return MemoryMapping(memory.m_id, mem, m_id);
	}

	void execute(const Kernel &kernel, std::size_t work_group_size, std::size_t total_size)
	{
		// Execute the kernel over the entire range of our 1d input data set
		// using the maximum number of work group items for this device
		//
		int err = clEnqueueNDRangeKernel(m_id, kernel.m_id, 1, NULL, &total_size, &work_group_size, 0, NULL, NULL);
		if (err)
		{
			throw std::runtime_error("Error: Failed to execute kernel!");
		}
  }

  void execute(const Kernel &kernel, std::size_t total_size)
  {
		// Execute the kernel over the entire range of our 1d input data set
		// using the maximum number of work group items for this device
		//
    int err = clEnqueueNDRangeKernel(m_id, kernel.m_id, 1, NULL, &total_size, NULL, 0, NULL, NULL);
		if (err)
    {
      throw std::runtime_error("Error: Failed to execute kernel!");
    }
  }

	Event execute_with_event(Kernel &kernel, std::size_t total_size)
	{
		// Execute the kernel over the entire range of our 1d input data set
		// using the maximum number of work group items for this device
		//
    Event event;
		int err = clEnqueueNDRangeKernel(m_id, kernel.m_id, 1, NULL, &total_size, NULL, 0, NULL, &event.m_id);
		if (err)
		{
			throw std::runtime_error("Error: Failed to execute kernel!");
		}
    return event;
	}

  /// Wait for the command commands to get serviced before reading back results
  void finish()
  {
    clFinish(m_id);
  }

private:
  friend class Context;
};

class Program : public ClObject<Program, cl_program>
{
public:
  Program(cl_device_id device, IdType id)
  : ClObject(id)
	, m_device(device)
  {
  }

  void release()
  {
    clReleaseProgram(m_id);
  }

  // Create the compute kernel in the program we wish to run
  Kernel create_kernel(const char *name)
  {
    int err;
    Kernel kernel{ m_device, clCreateKernel(m_id, name, &err) };
    if (!kernel.m_id || err != CL_SUCCESS)
    {
      throw std::runtime_error("Error: Failed to create compute kernel!");
    }
    return kernel;
  }

private:
  /// Build the program executable
  void build()
  {
    int err = clBuildProgram(
      m_id,
      0,
      NULL,
      NULL,
      NULL,
      NULL);
    if (err != CL_SUCCESS)
    {
      std::size_t len = 0;

      std::size_t log_size;
      clGetProgramBuildInfo(m_id, m_device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
      std::vector<char> buffer(log_size+1);

      std::cerr << "Error: Failed to build program executable!" << std::endl;
      clGetProgramBuildInfo(m_id, m_device, CL_PROGRAM_BUILD_LOG, log_size, buffer.data(), &len);
			buffer.back() = '\0';

      std::cerr << buffer.data() << std::endl;
      throw std::runtime_error("Shader compilation failure");
    }
  }

  cl_device_id m_device;
  friend class Context;
};

class Context : public ClObject<Context, cl_context>
{
public:
  Context(cl_device_id device, IdType id)
  : ClObject(id)
	, m_device(device)
  {
  }

  void release()
  {
    clReleaseContext(m_id);
  }

  /// Create a command commands
  CommandQueue create_command_queue()
  {
    int err;
		CommandQueue queue{ clCreateCommandQueue(m_id, m_device, 0, &err) };
    //CommandQueue queue{ clCreateCommandQueueWithProperties(m_id, m_device, 0, &err) };
    if (!queue.m_id)
    {
      throw std::runtime_error("Error: Failed to create a command commands!");
    }
    return queue;
  }

  // Create the compute program from the source buffer
  Program create_program(const std::vector<const char *> &source)
  {
    int err;
    Program program{ m_device, clCreateProgramWithSource(
      m_id,
      (cl_uint)source.size(),
      (const char **)source.data(),
      NULL,
      &err) };
    if (!program.m_id)
    {
      throw std::runtime_error("Error: Failed to create compute program!");
    }

    program.build();
    return program;
  }

	Memory create_buffer(cl_mem_flags flags, std::size_t size)
	{
		int err;
		Memory mem{ clCreateBuffer(m_id, flags, size, NULL, &err) };
		if (!mem.m_id)
		{
			throw std::runtime_error("Error: Failed to create memory buffer!");
		}
		return mem;
	}

private:
  cl_device_id m_device;
  friend class Device;
};

class Device : public ClObject<Device, cl_device_id>
{
public:
	enum class DeviceType
	{
		GPU,
		CPU
	};
  // Get the default compute device
  static Device get_default(DeviceType type = DeviceType::GPU)
  {
		cl_platform_id platforms[8];
		cl_uint size = 0;
		clGetPlatformIDs(8, platforms, &size);
		if (size == 0)
		{
			throw std::runtime_error("Error: No platforms available");
		}

#if 0
    const cl_platform_info attributeTypes[5] = {
      CL_PLATFORM_NAME,
      CL_PLATFORM_VENDOR,
      CL_PLATFORM_VERSION,
      CL_PLATFORM_PROFILE,
      CL_PLATFORM_EXTENSIONS
    };


    const char *attributeNames[5] = {
      "CL_PLATFORM_NAME",
      "CL_PLATFORM_VENDOR",
      "CL_PLATFORM_VERSION",
      "CL_PLATFORM_PROFILE",
      "CL_PLATFORM_EXTENSIONS"
    };

    for (std::size_t i = 0; i < size; ++i)
    {
      std::cout << "Platform - " << i+1 << std::endl;

      for (std::size_t j = 0; j < sizeof(attributeTypes)/sizeof(attributeTypes[0]); j++)
      {
        std::size_t info_size = 0;
        // get platform attribute value size
        clGetPlatformInfo(platforms[i], attributeTypes[j], 0, NULL, &info_size);
        auto info = (char*) malloc(info_size);

        // get platform attribute value
        clGetPlatformInfo(platforms[i], attributeTypes[j], info_size, info, NULL);

        std::cout << i+1 << "-" <<  j+1 << ": " << attributeNames[j] << (const char *)info << std::endl;
      }
    }
#endif

		cl_device_id id;
    int err = clGetDeviceIDs(platforms[0], type == DeviceType::GPU ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, 1, &id, NULL);
    if (err != CL_SUCCESS)
    {
      throw std::runtime_error("Error: Failed to create a device group!");
    }
		Device dev{ id };
    return dev;
  }

  void release()
  {
    clReleaseDevice(m_id);
  }

  /// Create a compute context
  Context create_context()
  {
    int err;
    Context context{ m_id, clCreateContext(0, 1, &m_id, NULL, NULL, &err) };
    if (!context.m_id)
    {
      throw std::runtime_error("Error: Failed to create a compute context!");
    }
    return context;
  }

private:
	Device(IdType id)
	: ClObject(id)
	{
	}
};

}

#endif
