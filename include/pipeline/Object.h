#pragma once

#include "pipeline/Memory/MemoryReference.h"
#include "pipeline/Type.h"

namespace pipeline
{

/// Holds a memory reference and type pair, representing a thing in memory
class Object
{
public:
  /// Create an emtpy object
  Object()
  : m_type(nullptr)
  {
  }

  /// Initialise a real object from memory and type
  static Object initialise(MemoryReference mem, const Type *type)
  {
    return Object{ std::move(mem), type };
  }

  /// Find an objects type
  const Type *type() { return m_type; }

  /// Get an objects memory
	const void *get() const
	{
		return m_memory.get();
	}

  /// Get an objects memory
	void *get()
	{
		return m_memory.get();
	}

private:
  Object(MemoryReference mem, const Type *type)
  : m_memory(mem)
  , m_type(type)
  {
  }

	MemoryReference m_memory;
  const Type *m_type;
};

}
