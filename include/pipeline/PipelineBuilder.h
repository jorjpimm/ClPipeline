#pragma once

#include "pipeline/Computation/Computation.h"
#include "pipeline/ObjectLocation.h"

#include <gsl/gsl-lite.h>
#include <vector>

namespace pipeline
{

/// Build pipelines for executions.
/// Allows adding calls, arguments and outputs to the builder.
class PipelineBuilder
{
public:
  /// A call in the pipeline, to a [Computation] object.
  class PipelineCall
  {
  public:
		PipelineCall(
      const Computation *function,
      std::vector<ObjectLocation> arguments);

    /// Get the list of objects used as arguments to the call
    gsl::span<const ObjectLocation> arguments() const { return const_cast<decltype(m_arguments)&>(m_arguments); }
    /// Get the function to call
    const Computation *function() const { return m_function; }

  private:
    std::vector<ObjectLocation> m_outputs;
    std::vector<ObjectLocation> m_arguments;
    const Computation *m_function;
  };

  /// Create an empty pipeline
  PipelineBuilder();

  /// Add a new call to the pipeline.
  /// \param computation The [Computation] to call when executing the pipeline.
  /// \param inputs      The inputs to the computation
  /// \param outputs     The outputs of the function.
  std::shared_ptr<PipelineCall> add_call(
    const Computation &computation,
    const std::vector<ObjectLocation> &inputs,
    std::vector<ObjectLocation> &outputs);

  /// Add an argument to the pipeline of [type].
  /// \param type         The type of the argument to create
  /// \param alloc_strat  The allocation strategy to use for the argument
  ObjectLocation add_argument(
    const Type *type,
    ObjectLocation::AllocationStrategy alloc_strat);

  /// Define a given pipeline object [output] as an output of the pipeline.
	ObjectLocation add_output(ObjectLocation output);

	/// Find a list of objects of the pipeline.
	gsl::span<const ObjectLocation> objects() const { return m_all_objects; }
	/// Find a list of arguments of the pipeline.
	gsl::span<const ObjectLocation> arguments() const { return m_arguments; }
	/// Find a list of outputs of the pipeline.
	gsl::span<const ObjectLocation> outputs() const { return m_outputs; }
  /// Find a list of calls in the pipeline.
	gsl::span<const std::shared_ptr<PipelineCall>> calls() const { return m_calls; }

private:
  ObjectLocation::Id make_next_id();

  ObjectLocation::Id m_next_id;
  std::vector<std::shared_ptr<PipelineCall>> m_calls;
  std::vector<ObjectLocation> m_all_objects;
	std::vector<ObjectLocation> m_arguments;
	std::vector<ObjectLocation> m_outputs;
};

}
