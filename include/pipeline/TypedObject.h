#pragma once

#include "pipeline/Object.h"
#include "pipeline/TypeFinder.h"

namespace pipeline
{

/// Holds a memory reference and type pair, with a C++ type reference,
/// representing a T in memory
template <typename T> class TypedObject : public Object
{
public:
  /// Initialise an object from a piece of memory
  static TypedObject<T> initialise(MemoryReference mem)
  {
    auto type = static_type();
    auto obj = Object::initialise(std::move(mem), type);
    return from(obj);
  }

  /// Create a typed object from a non-typed object
  static TypedObject<T> from(Object obj)
  {
    assert(obj.get());
    assert(obj.type() == static_type());
    return TypedObject<T>(std::move(obj));
  }

  /// Cast a typed object from a non-typed object
  /// (refers to the original <Object> in memory)
  static TypedObject<T> &cast(Object &obj)
  {
    assert(obj.get());
    assert(obj.type() == static_type());
    return static_cast<TypedObject<T>&>(obj);
  }

  /// Get the objects memory as a [const T &]
	const T &get() const
	{
		return *static_cast<const T*>(Object::get());
	}

  /// Get the objects memory as a [T &]
	T &get()
  {
		return *static_cast<T*>(Object::get());
	}

  /// Access the objects memory
	T *operator->()
	{
		return &get();
	}

  /// Access the objects memory
	const T *operator->() const
	{
		return &get();
	}

  /// Find the type of this object
  static const Type *static_type() { return TypeFinder<T>::get(); }

private:
  TypedObject(Object obj)
  : Object(std::move(obj))
  {
  }
};

}
