#pragma once

#include "pipeline/Computation/Computation.h"
#include "pipeline/ObjectArray.h"
#include "pipeline/TypedObject.h"

#include <functional>

namespace pipeline
{

/// A computation evaluated on the local cpu
class LocalComputation : public Computation
{
public:
  class MultiStreamArguments
  {
  public:
    MultiStreamArguments(
      gsl::span<ObjectArray *> arguments)
    : m_arguments(arguments)
    {
    }

    ObjectArray &at(std::size_t argument) const
    {
      assert(argument < m_arguments.size());
      return *m_arguments[argument];
    }

    template <typename T> TypedObjectArray<T> &at(std::size_t argument) const
    {
      return TypedObjectArray<T>::cast(at(argument));
    }


    ObjectArray &operator[](std::size_t i) const { return at(i); }

  private:
    gsl::span<ObjectArray *> m_arguments;
  };

  /// Create a new computation from a functor
	LocalComputation(
		std::initializer_list<const Type *> inputs,
		std::initializer_list<Output> outputs,
    EvaluationStrategy strat)
  : Computation(inputs, outputs, strat, ComputationType::Local)
	{
	}

  /// Invoke the computation
  virtual void call(std::size_t inv_id, const MultiStreamArguments &args) const = 0;
};


class LocalPerStreamComputation : public LocalComputation
{
public:
  class Arguments : MultiStreamArguments
  {
  public:
    Arguments(
      const MultiStreamArguments &args,
      std::size_t stream)
    : MultiStreamArguments(args)
    , m_stream(stream)
    {
    }

    Object at(std::size_t argument) const
    {
      return MultiStreamArguments::at(argument).at_stream(m_stream);
    }

    Object operator[](std::size_t i) const { return at(i); }

    template <typename T> TypedObject<T> at(std::size_t i) const
    {
      return TypedObject<T>::from(at(i));
    }

  private:
    std::size_t m_stream;
  };

  using PerStreamFunctionType =void(*)(const Arguments &);

  /// Create a new computation from a functor
	LocalPerStreamComputation(
		std::initializer_list<const Type *> inputs,
		std::initializer_list<Output> outputs,
		PerStreamFunctionType fn)
  : LocalComputation(inputs, outputs, Computation::EvaluationStrategy::PerStream)
	, m_function(fn)
	{
  }

  /// Invoke the computation
	void call(std::size_t inv_id, const MultiStreamArguments &mul_args) const override
	{
    Arguments args(mul_args, inv_id);
		m_function(args);
	}

private:
  PerStreamFunctionType m_function;
};

class LocalSingleComputation : public LocalComputation
{
public:
  using SingleFunctionType =void(*)(const MultiStreamArguments &);

  /// Create a new computation from a functor
	LocalSingleComputation(
		std::initializer_list<const Type *> inputs,
		std::initializer_list<Output> outputs,
		SingleFunctionType fn)
  : LocalComputation(inputs, outputs, Computation::EvaluationStrategy::Single)
	, m_function(fn)
	{
  }

  /// Invoke the computation
	void call(std::size_t inv_id, const MultiStreamArguments &args) const override
	{
		m_function(args);
	}

private:
  SingleFunctionType m_function;
};

}
