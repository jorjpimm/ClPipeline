#pragma once

#include "pipeline/Computation/Computation.h"

#ifdef PIPELINE_ENABLE_OPENCL

#include "pipeline/detail/cl.h"

namespace pipeline
{

class ClComputation : public Computation
{
public:
	ClComputation(
		cl::Context &context,
		std::initializer_list<const Type *> inputs,
    std::initializer_list<Output> outputs,
    const std::string &kernel_name,
		const std::string &source)
  : Computation(
    inputs,
    outputs,
    Computation::EvaluationStrategy::PerStream,
    ComputationType::CL)
	, m_program(context.create_program({ source.c_str() }))
  , m_kernel(m_program.create_kernel(kernel_name.c_str()))
	{
	}

  const cl::Program &program() const { return m_program; }
  cl::Kernel &kernel() const { return m_kernel; }

private:
	cl::Program m_program;
  mutable cl::Kernel m_kernel;
};


}

#endif