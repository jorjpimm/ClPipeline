#pragma once

#include "pipeline/ObjectLocation.h"
#include "pipeline/Type.h"

namespace pipeline
{

/// Base for computable types
class Computation
{
public:
  enum class EvaluationStrategy : std::uint8_t
  {
    Single,
    PerStream
  };

  /// All computable types
  enum class ComputationType
  {
    Local,
    CL,
		Unknown
  };

  struct Output
  {
    Output(
      const Type *t,
      ObjectLocation::AllocationStrategy s = ObjectLocation::AllocationStrategy::PerStream)
    : type(t)
    , strategy(s)
    {
    }

    bool operator==(const Output &o) const
    {
      return type == o.type && strategy == o.strategy;
    }

    const Type *type;
    ObjectLocation::AllocationStrategy strategy;
  };

  /// Create a new computation
  /// \param inputs   Inputs to the computation
  /// \param outputs  Outputs from the computation
  /// \param type     Type of the computation
  Computation(
    std::initializer_list<const Type *> inputs,
    std::initializer_list<Output> outputs,
    EvaluationStrategy strat,
    ComputationType type)
  : m_inputs(inputs)
  , m_outputs(outputs)
  , m_strategy(strat)
  , m_type(type)
  {
  }

  virtual ~Computation() {}

  /// Inputs to the computation
  gsl::span<const Type *> inputs() const { return gsl::as_span(const_cast<decltype(m_inputs)&>(m_inputs)); }
  /// Outputs from the computation
  gsl::span<Output> outputs() const { return gsl::as_span(const_cast<decltype(m_outputs)&>(m_outputs)); }

  /// How should the computation be evaluated
  EvaluationStrategy strategy() const { return m_strategy; }

  /// Type of the computation
  ComputationType type() const { return m_type; }

private:
  std::vector<const Type *> m_inputs;
  std::vector<Output> m_outputs;
  EvaluationStrategy m_strategy;
  ComputationType m_type;
};

}
