#pragma once

#include "pipeline/Object.h"
#include "pipeline/TypedObject.h"
#include "pipeline/Type.h"

namespace pipeline
{

/// Holds a memory reference and type pair, representing a thing in memory
class ObjectArray
{
public:
  struct iterator
  {
    const Type *type;
    MemoryReference data;
    MemoryDescription::MemoryOffset stride;
    std::size_t index;

    bool operator!=(iterator it) const
    {
      return index != it.index;
    }

    bool operator==(iterator it) const
    {
      return index == it.index;
    }

    iterator &operator++()
    {
      data += stride;
      index += 1;
      return *this;
    }

    iterator operator+(std::size_t count) const
    {
      return iterator{ type, data + stride * count, stride, index + count };
    }

    Object operator*() const
    {
      return Object::initialise(data, type);
    }
  };

  /// Create an emtpy object
  ObjectArray()
  : m_type(nullptr)
  , m_data()
  , m_stream_count(0)
  , m_count(0)
  , m_stride(0)
  {
  }

  /// Initialise a real object from memory and type
  static ObjectArray initialise(
    const Type *type,
    MemoryReference data,
    std::size_t stream_count,
    std::size_t count,
    MemoryDescription::MemoryOffset stride)
  {
    return ObjectArray{ type, data, stream_count, count, stride };
  }

  iterator begin()
  {
    return { m_type, m_data, m_stride, 0 };
  }

  iterator end()
  {
    return { m_type, m_data + (m_stride * m_count), m_stride, m_count };
  }

	Object at(std::size_t i) const
	{
		assert(i < m_count);
		auto data = m_data + (m_stride * i);
		return Object::initialise(data, m_type);
	}

	Object at_stream(std::size_t i) const
	{
		assert(i < m_stream_count);
		auto data = m_data + (m_stride * i);
		return Object::initialise(data, m_type);
	}

  std::size_t size() const
  {
    return m_count;
  }

  std::size_t stream_count() const
  {
    return m_stream_count;
  }

private:
  ObjectArray(
    const Type *type,
    MemoryReference data,
    std::size_t stream_count,
    std::size_t count,
    MemoryDescription::MemoryOffset stride)
  : m_type(type)
  , m_data(data)
  , m_stream_count(stream_count)
  , m_count(count)
  , m_stride(stride)
  {
  }

  const Type *m_type;
  MemoryReference m_data;
  std::size_t m_stream_count;
  std::size_t m_count;
  MemoryDescription::MemoryOffset m_stride;
};

template <typename T> class TypedObjectArray : public ObjectArray
{
public:
  static TypedObjectArray<T> from(const ObjectArray &arr)
  {
    return TypedObjectArray<T>(arr);
  }

  static TypedObjectArray<T> &cast(ObjectArray &arr)
  {
    return static_cast<TypedObjectArray<T>&>(arr);
  }

	TypedObject<T> at(std::size_t i) const
	{
		return TypedObject<T>::from(ObjectArray::at(i));
	}

	TypedObject<T> at_stream(std::size_t i) const
	{
		return TypedObject<T>::from(ObjectArray::at_stream(i));
	}
private:
  TypedObjectArray(const ObjectArray &arr)
  : ObjectArray(arr)
  {
  }
};

}
