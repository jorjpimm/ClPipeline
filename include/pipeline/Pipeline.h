#pragma once

#include "pipeline/Memory/Memory.h"
#include "pipeline/PipelineBuilder.h"

namespace pipeline
{

/// A pipeline contains a list of functions to invoke in sequence on input arguments
template <typename MemoryLayout, typename Queue> class Pipeline
{
public:
  /// Create a new pipeline
  /// \param builder  The builder object to create a pipeline from
  /// \param alloc    The allocator to use to create pipeline memory
  /// \param queue    The work queue to append work items to
  /// \param streams  The number of streams to handle in the pipeline
	template <typename Allocator> Pipeline(
    PipelineBuilder builder,
    Allocator &alloc,
    Queue &queue,
    std::size_t streams)
  : m_stream_count(streams)
  , m_layout(builder.objects(), m_stream_count)
  , m_memory(m_layout.create(alloc))
	, m_queue(queue)
	{
		auto calls = builder.calls();
		m_calls.assign(calls.begin(), calls.end());
	}

  /// Get an object from the pipelines memory
	typename MemoryLayout::ObjectArrayType get_object_array(
    MemoryMapType type,
    ObjectLocation obj)
  {
    return m_layout.get_object_array(type, m_memory, obj);
  }

  /// Run the pipeline with setup arguments (see get_object)
  void run()
  {
		for (auto &call : m_calls)
		{
			m_queue.enqueue(*call, m_stream_count, m_layout, m_memory);
		}

		m_queue.block_for_finish();
  }
  
  /// Get the layout held by the pipeline
  const MemoryLayout &layout() const
  {
  return m_layout;
  }
  
  /// Get the memory held by the pipeline
  const typename MemoryLayout::Memory &memory() const
  {
    return m_memory;
  }

private:
  std::size_t m_stream_count;
	MemoryLayout m_layout;
	typename MemoryLayout::Memory m_memory;

	std::vector<std::shared_ptr<PipelineBuilder::PipelineCall>> m_calls;
	Queue &m_queue;
};

}
