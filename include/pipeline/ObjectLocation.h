#pragma once

#include <cstddef>
#include <cstdint>
#include <limits>

namespace pipeline
{

class Type;

/// The location of object in a pipeline
class ObjectLocation
{
public:
  using Id = std::uint16_t;
  enum class AllocationStrategy : std::uint8_t
  {
    Single,
    PerStream
  };

  /// Create a null pipeline object
  ObjectLocation()
  : m_type(nullptr)
  , m_id(std::numeric_limits<Id>::max())
  , m_strategy(AllocationStrategy::PerStream)
  {
  }

  /// Create a new pipeline object location with a given [id] and [type]
  ObjectLocation(
    Id id,
    const Type *type,
    AllocationStrategy strategy)
  : m_type(type)
  , m_id(id)
  , m_strategy(strategy)
  {
  }

  /// Find the id of the object
  Id id() const { return m_id; }
  /// Find the type of the object
  const Type *type() const { return m_type; }

  bool is_per_stream() const { return m_strategy == AllocationStrategy::PerStream; }

	bool operator==(const ObjectLocation &obj) const
	{
		return m_id == obj.m_id;
	}

private:
  const Type *m_type;
  Id m_id;
  AllocationStrategy m_strategy;
};

}
