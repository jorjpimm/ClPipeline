#pragma once

#include "pipeline/Memory/MemoryDescription.h"
#include "pipeline/Memory/MemoryReference.h"
#include <gsl/gsl-lite.h>
#include <type_traits>

namespace pipeline
{

/// A reflectable pipeline type
class Type
{
public:
  using CreateFn = void(*)(void *);
  using DestroyFn = void(*)(void *);

  /// Create a new type from memory description and name
  Type(
    const MemoryDescription &desc,
    const char *name,
    CreateFn ctor,
    DestroyFn dtor)
  : m_memory_desc(desc)
  , m_name(name)
  , m_ctor(ctor)
  , m_dtor(dtor)
  {
    if (!ctor || !dtor)
    {
      throw std::runtime_error("Expected construct and descructor pair");
    }
  }

  /// Create for a given type
  template <typename T> static Type create(const char *name)
  {
    if (std::is_trivial<T>::value)
    {
      return Type(MemoryDescription::for_type<T>(), name, noop, noop);
    }

    struct Utils
    {
      static void create(void *data)
      {
        auto obj = static_cast<T*>(data);
        new(obj) T();
      }
      static void destroy(void *data)
      {
        auto obj = static_cast<T*>(data);
        obj->~T();
      }
    };

    return Type(MemoryDescription::for_type<T>(), name, Utils::create, Utils::destroy);
  }

  /// Find the types name
  const std::string &name() const { return m_name; }

  /// Find the memory description for a type
	const MemoryDescription &memory_description() const { return m_memory_desc; }

  bool is_trivial() const
  {
    return m_ctor == noop && m_dtor == noop;
  }

  void emplace(MemoryReference ref) const
  {
    m_ctor(ref.get());
  }

  void destroy(MemoryReference ref) const
  {
    m_dtor(ref.get());
  }

  /// Find sub objects contained in the type
  /// intended for use when serialising or deserialsing objects
  /// TODO: type members
  // gsl::span<Member> members() const;

private:
	static void noop(void *data) { }

	MemoryDescription m_memory_desc;
  std::string m_name;

  CreateFn m_ctor;
  DestroyFn m_dtor;
};

}
