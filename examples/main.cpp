
void example_local_pipeline();
void example_multi_width_pipeline();
void example_cl_pipeline();
void example_mixed_pipeline();

int main(int argc, char** argv)
{
  //example_local_pipeline();
  example_multi_width_pipeline();

#ifdef PIPELINE_ENABLE_OPENCL
  //example_cl_pipeline();
  //example_mixed_pipeline();
#endif
}
