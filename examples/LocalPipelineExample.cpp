#include "pipeline/Memory/LocalMemory.h"
#include "pipeline/Memory/Layout/AOSMemoryLayout.h"
#include "pipeline/Pipeline.h"
#include "pipeline/PipelineBuilder.h"
#include "pipeline/Queue/LocalQueue.h"
#include "pipeline/TypeFinder.h"
#include "pipeline/TypedObject.h"
#include "pipeline/Computation/LocalComputation.h"

#include <iostream>

// A local structure we will use as arguments to pipeline steps
struct LocalTest
{
  int val;
};

// This defines declaration for TypeFinder<LocalTest>
PIPELINE_DEFINE_TYPE(LocalTest)

// This defines definition for TypeFinder<LocalTest>
PIPELINE_IMPLEMENT_TYPE(LocalTest)

void example_local_pipeline()
{
  // First lookup the type we created above
  auto test_type = pipeline::TypeFinder<LocalTest>::get();

  // Define a local computation
  auto sum = pipeline::LocalPerStreamComputation(
    // Takes two arguments of the same type
    { test_type, test_type },
    // returns one argument, of the same type
    { test_type },
    // Implementation of the computation
    [](const pipeline::LocalPerStreamComputation::Arguments &args)
    {
      // Extract argument values
      auto a = args.at<LocalTest>(0);
      auto b = args.at<LocalTest>(1);
      auto out = args.at<LocalTest>(2);

      // Sum the inputs
      out->val = a->val + b->val;
    }
  );

  // This is a lightweight wrapper around allocation, does
  // very little when using local memory...
	struct {
		pipeline::LocalMemory allocate(const pipeline::MemoryDescription &desc)
		{
			return pipeline::LocalMemory::create(desc);
		}
	} memory_allocator;

  // Now define the pipeline structure:
  auto pipeline = pipeline::PipelineBuilder();

  // We have 3 overall inputs to the pipeline
  auto arg0 = pipeline.add_argument(test_type, pipeline::ObjectLocation::AllocationStrategy::PerStream);
  auto arg1 = pipeline.add_argument(test_type, pipeline::ObjectLocation::AllocationStrategy::PerStream);
  auto arg2 = pipeline.add_argument(test_type, pipeline::ObjectLocation::AllocationStrategy::PerStream);

  // Container for outputs of calls
  std::vector<pipeline::ObjectLocation> outputs;

  // Add a call to the pipeline - arguments from above
  auto call1 = pipeline.add_call(sum, { arg0, arg1 }, outputs);
  // Add a second call, arguments from arg defs, and the output of the previous call
  auto call2 = pipeline.add_call(sum, { outputs[0], arg2 }, outputs);

  // add an output from the pipeline, to be available once it has run
  auto output = pipeline.add_output(outputs[0]);


  // Defining the type of memory layout we use in the pipeline - Array of Structures
  // This clusters all the memory for a stream together
	using MemoryType = pipeline::AOSMemoryLayout<pipeline::LocalMemory>;
  // Defining the type of queue - right now - local evaluation
  using QueueType = pipeline::LocalQueue<MemoryType>;
	QueueType queue;

  // Create the pipeline
  auto pipeline_instance = pipeline::Pipeline<MemoryType, QueueType>(
    // Builder for the pipeline
    pipeline,
    // Allocator for the pipeline memory
    memory_allocator,
    // Queue to evaulate the pipeline on
    queue,
    // Stream count
    500);

  auto arg0_arr = pipeline::TypedObjectArray<LocalTest>::from(pipeline_instance.get_object_array(pipeline::MemoryMapType::Write, arg0));
	auto arg1_arr = pipeline::TypedObjectArray<LocalTest>::from(pipeline_instance.get_object_array(pipeline::MemoryMapType::Write, arg1));
	auto arg2_arr = pipeline::TypedObjectArray<LocalTest>::from(pipeline_instance.get_object_array(pipeline::MemoryMapType::Write, arg2));

  // Set up the pipeline with input arguments
  for (std::size_t i = 0; i < 500; ++i)
  {
		arg0_arr.at_stream(i)->val = 5;
		arg1_arr.at_stream(i)->val = 4;
		arg2_arr.at_stream(i)->val = 3;
  }

  // Run the pipeline on the inputs
	pipeline_instance.run();

  // Extract the output
  auto out_arr = pipeline::TypedObjectArray<LocalTest>::from(pipeline_instance.get_object_array(pipeline::MemoryMapType::Read, output));
  for (std::size_t i = 0; i < 500; ++i)
  {
		auto out = out_arr.at_stream(i)->val;
    std::cout << i << " " << out << std::endl;
  }
}
