#include "pipeline/Memory/LocalMemory.h"
#include "pipeline/Memory/Layout/SOAMemoryLayout.h"
#include "pipeline/Pipeline.h"
#include "pipeline/PipelineBuilder.h"
#include "pipeline/Queue/LocalQueue.h"
#include "pipeline/TypeFinder.h"
#include "pipeline/TypedObject.h"
#include "pipeline/Computation/LocalComputation.h"

#include <iostream>

namespace {

// A local structure we will use as arguments to pipeline steps
struct StreamData
{
  int val;
};

// A local structure we will use as arguments to pipeline steps
struct Summary
{
  int sum;
};

// A config structure we will use input to pipeline steps
struct Config
{
  int multiplier;
};

}

// This defines declaration for TypeFinder<*>
PIPELINE_DEFINE_TYPE(StreamData)
PIPELINE_DEFINE_TYPE(Summary)
PIPELINE_DEFINE_TYPE(Config)

// This defines definition for TypeFinder<*>
PIPELINE_IMPLEMENT_TYPE(StreamData)
PIPELINE_IMPLEMENT_TYPE(Summary)
PIPELINE_IMPLEMENT_TYPE(Config)

void example_multi_width_pipeline()
{
  // First lookup the type we created above
  auto config_type = pipeline::TypeFinder<Config>::get();
  auto summary_type = pipeline::TypeFinder<Summary>::get();
  auto stream_type = pipeline::TypeFinder<StreamData>::get();

  // Takes one config input, and adapts it.
  auto mutate_config = pipeline::LocalSingleComputation(
    // Takes two arguments of the same type
    { config_type },
    // returns one argument, of the same type
    { pipeline::LocalSingleComputation::Output(config_type, pipeline::ObjectLocation::AllocationStrategy::Single) },
    // Implementation of the computation
    [](const pipeline::LocalComputation::MultiStreamArguments &args)
    {
      // Extract argument values
      auto inp = args.at<Config>(0).at(0);
      auto out = args.at<Config>(1).at(0);

      // Sum the inputs
      out->multiplier = inp->multiplier * 5;
    }
  );

  // takes streams and a config and combines them
  auto multiply_streams = pipeline::LocalPerStreamComputation(
    // Takes two arguments of the same type
    { stream_type, config_type },
    // returns one argument, of the same type
    { stream_type },
    // Implementation of the computation
    [](const pipeline::LocalPerStreamComputation::Arguments &args)
    {
      // Extract argument values
      auto str = args.at<StreamData>(0);
      auto cfg = args.at<Config>(1);
      auto out = args.at<StreamData>(2);

      // Sum the inputs
      out->val = str->val * cfg->multiplier;
    }
  );

  // Takes all stream inputs, and reorders them
  auto reorder_streams = pipeline::LocalSingleComputation(
    // Takes two arguments of the same type
    { stream_type },
    // returns one argument, of the same type
    { stream_type },
    // Implementation of the computation
    [](const pipeline::LocalComputation::MultiStreamArguments &args)
    {
      // Extract argument values
      auto str = args.at<StreamData>(0);
      auto out = args.at<StreamData>(1);

      // reverse the inputs.
      for (auto stream = 0; stream < str.stream_count(); ++stream)
      {
        auto adjusted_stream = str.stream_count() - stream - 1;
        out.at_stream(adjusted_stream)->val = str.at_stream(stream)->val;
      }
    }
  );

  // Takes all stream inputs and aggregates them
  auto sumarise_streams = pipeline::LocalSingleComputation(
    // Takes two arguments of the same type
    { stream_type },
    // returns one argument, of the same type
    { summary_type },
    // Implementation of the computation
    [](const pipeline::LocalComputation::MultiStreamArguments &args)
    {
      // Extract argument values
      auto str = args.at<StreamData>(0);
      auto out = args.at<Summary>(1).at(0);

      // reverse the inputs.
      for (auto stream = 0; stream < str.stream_count(); ++stream)
        {
        out->sum += str.at_stream(stream)->val;
      }
    }
  );

  // This is a lightweight wrapper around allocation, does
  // very little when using local memory...
	struct {
		pipeline::LocalMemory allocate(const pipeline::MemoryDescription &desc)
		{
			return pipeline::LocalMemory::create(desc);
		}
	} memory_allocator;

  // Now define the pipeline structure:
  auto pipeline = pipeline::PipelineBuilder();

  // We have 3 overall inputs to the pipeline
  auto arg0 = pipeline.add_argument(config_type, pipeline::ObjectLocation::AllocationStrategy::Single);
  auto arg1 = pipeline.add_argument(stream_type, pipeline::ObjectLocation::AllocationStrategy::PerStream);

  // Container for outputs of calls
  std::vector<pipeline::ObjectLocation> outputs;

  auto call1 = pipeline.add_call(mutate_config, { arg0 }, outputs);
  auto call2 = pipeline.add_call(multiply_streams, { arg1, outputs[0] }, outputs);
  auto call3 = pipeline.add_call(reorder_streams, { outputs[0] }, outputs);
  auto final_output_0 = pipeline.add_output(outputs[0]);
  auto call4 = pipeline.add_call(sumarise_streams, { outputs[0] }, outputs);
  auto final_output_1 = pipeline.add_output(outputs[0]);



  // Defining the type of memory layout we use in the pipeline - Array of Structures
  // This clusters all the memory for a stream together
	using MemoryType = pipeline::SOAMemoryLayout<pipeline::LocalMemory>;
  // Defining the type of queue - right now - local evaluation
  using QueueType = pipeline::LocalQueue<MemoryType>;
	QueueType queue;

  // Create the pipeline
  auto pipeline_instance = pipeline::Pipeline<MemoryType, QueueType>(
    // Builder for the pipeline
    pipeline,
    // Allocator for the pipeline memory
    memory_allocator,
    // Queue to evaulate the pipeline on
    queue,
    // Stream count
    500);

  auto arg0_sing = pipeline::TypedObjectArray<Config>::from(
    pipeline_instance.get_object_array(pipeline::MemoryMapType::Write, arg0));
	auto arg1_arr = pipeline::TypedObjectArray<StreamData>::from(
    pipeline_instance.get_object_array(pipeline::MemoryMapType::Write, arg1));

  arg0_sing.at(0)->multiplier = 2;

  // Set up the pipeline with input arguments
  for (std::size_t i = 0; i < 500; ++i)
  {
		arg1_arr.at_stream(i)->val = i;
  }

  // Run the pipeline on the inputs
	pipeline_instance.run();

  // Extract the output
	auto sumary_sing = pipeline::TypedObjectArray<Summary>::from(
    pipeline_instance.get_object_array(pipeline::MemoryMapType::Write, final_output_1));
  std::cout << "total sum " << sumary_sing.at(0)->sum << std::endl;
  
  auto str_out_arr = pipeline::TypedObjectArray<StreamData>::from(
    pipeline_instance.get_object_array(pipeline::MemoryMapType::Read, final_output_0));
  for (std::size_t i = 0; i < 500; ++i)
  {
    auto out = str_out_arr.at_stream(i)->val;
    std::cout << i << " " << out << std::endl;
  }
}
