#ifdef PIPELINE_ENABLE_OPENCL

#include "pipeline/Memory/ClMemory.h"
#include "pipeline/Memory/Layout/SOAMemoryLayout.h"
#include "pipeline/Pipeline.h"
#include "pipeline/PipelineBuilder.h"
#include "pipeline/Queue/MixedQueue.h"
#include "pipeline/TypedObject.h"

struct MixedTest
{
  int val;
};

PIPELINE_DEFINE_TYPE(MixedTest)
PIPELINE_IMPLEMENT_TYPE(MixedTest)


void example_mixed_pipeline()
{
	auto device = cl::Device::get_default();
	auto context = device.create_context();


	using MemoryType = pipeline::SOAMemoryLayout<pipeline::ClMemory>;
  using QueueType = pipeline::MixedQueue<MemoryType>;
  QueueType queue(context);

  auto test_type = pipeline::TypedObject<MixedTest>::static_type();
  auto mul = pipeline::ClComputation(
		context,
    { test_type, test_type },
    { test_type },
    "run_kernel",
    R"CL(
struct Test
{
  int val;
};

__kernel void run_kernel(
   __global struct Test* input_a,
   __global struct Test* input_b,
	 __global struct Test* output)
	{
		int i = get_global_id(0);
		output[i].val = input_a[i].val * input_b[i].val;
	}
)CL"
  );

  auto sum = pipeline::LocalPerStreamComputation(
    { test_type, test_type },
    { test_type },
    [](const pipeline::LocalPerStreamComputation::Arguments &args)
    {
      auto a = args.at<MixedTest>(0);
      auto b = args.at<MixedTest>(1);
      auto out = args.at<MixedTest>(2);

      out->val = a->val + b->val;
    }
  );

	struct {
		pipeline::ClMemory allocate(const pipeline::MemoryDescription &desc)
		{
			auto buf = context.create_buffer(CL_MEM_READ_WRITE, desc.size());
			return pipeline::ClMemory::initialise(std::move(buf), desc.size(), &queue);
		}
		cl::Context &context;
		cl::CommandQueue &queue;
	} memory_allocator{ context, queue.cl_queue().cl_queue() };

  auto pipeline = pipeline::PipelineBuilder();
  auto arg0 = pipeline.add_argument(test_type, pipeline::ObjectLocation::AllocationStrategy::PerStream);
  auto arg1 = pipeline.add_argument(test_type, pipeline::ObjectLocation::AllocationStrategy::PerStream);
  auto arg2 = pipeline.add_argument(test_type, pipeline::ObjectLocation::AllocationStrategy::PerStream);
  auto arg3 = pipeline.add_argument(test_type, pipeline::ObjectLocation::AllocationStrategy::PerStream);
  std::vector<pipeline::ObjectLocation> outputs;
  auto call1 = pipeline.add_call(sum, { arg0, arg1 }, outputs);
  auto call2 = pipeline.add_call(mul, { outputs[0], arg2 }, outputs);
  auto call3 = pipeline.add_call(sum, { outputs[0], arg3 }, outputs);
  auto output = pipeline.add_output(outputs[0]);

  auto pipeline_instance = pipeline::Pipeline<MemoryType, QueueType>(pipeline, memory_allocator, queue, 500);

  auto arg0_arr = pipeline::TypedObjectArray<MixedTest>::from(pipeline_instance.get_object_array(pipeline::MemoryMapType::Write, arg0));
	auto arg1_arr = pipeline::TypedObjectArray<MixedTest>::from(pipeline_instance.get_object_array(pipeline::MemoryMapType::Write, arg1));
	auto arg2_arr = pipeline::TypedObjectArray<MixedTest>::from(pipeline_instance.get_object_array(pipeline::MemoryMapType::Write, arg2));
	auto arg3_arr = pipeline::TypedObjectArray<MixedTest>::from(pipeline_instance.get_object_array(pipeline::MemoryMapType::Write, arg3));

	for (std::size_t i = 0; i < 500; ++i)
	{
		arg0_arr.at_stream(i)->val = 5;
		arg1_arr.at_stream(i)->val = 4;
		arg2_arr.at_stream(i)->val = 3;
		arg3_arr.at_stream(i)->val = 2;
	}

	pipeline_instance.run();
  
  auto out_arr = pipeline::TypedObjectArray<MixedTest>::from(pipeline_instance.get_object_array(pipeline::MemoryMapType::Read, output));
	for (std::size_t i = 0; i < 500; ++i)
	{
		auto out = out_arr.at_stream(i)->val;
		std::cout << out << std::endl;
	}
}

#endif
