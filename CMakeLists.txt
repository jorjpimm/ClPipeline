cmake_minimum_required(VERSION 3.2)
enable_testing()
project(Pipeline)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/")

if (NOT WIN32)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
endif()

if (NOT DEFINED PIPELINE_OPENCL OR
    (PIPELINE_OPENCL AND NOT OPENCL_FOUND))

  find_package(OpenCL)

  if (OPENCL_FOUND)
    set(PIPELINE_OPENCL ON CACHE STRING "OpenCL Enabled")
  else()
    set(PIPELINE_OPENCL OFF CACHE STRING "OpenCL Enabled")
  endif()

else()
  set(PIPELINE_OPENCL ${PIPELINE_OPENCL} CACHE STRING "OpenCL Enabled")
endif()

if (PIPELINE_OPENCL)
  set(USE_OPENCL TRUE)
endif()

find_package(Threads REQUIRED)

add_library(pipeline
  include/pipeline/Object.h
  include/pipeline/ObjectLocation.h
  include/pipeline/Pipeline.h
  include/pipeline/PipelineBuilder.h
  include/pipeline/Type.h
  include/pipeline/TypedObject.h
  include/pipeline/TypeFinder.h

  include/pipeline/Computation/ClComputation.h
  include/pipeline/Computation/Computation.h
  include/pipeline/Computation/ClComputation.h

  include/pipeline/Memory/ClMemory.h
  include/pipeline/Memory/LocalMemory.h
  include/pipeline/Memory/Memory.h
  include/pipeline/Memory/MemoryDescription.h
  include/pipeline/Memory/MemoryReference.h
  include/pipeline/Memory/Layout/AOSMemoryLayout.h
	include/pipeline/Memory/Layout/SOAMemoryLayout.h

  include/pipeline/Queue/ClQueue.h
  include/pipeline/Queue/LocalQueue.h
  include/pipeline/Queue/MixedQueue.h
  include/pipeline/Queue/Queue.h

  include/pipeline/detail/cl.h

  src/PipelineBuilder.cpp
)

target_compile_options(pipeline
  PUBLIC
    -D_SCL_SECURE_NO_WARNINGS
)

target_include_directories(pipeline
  PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
    ${CMAKE_SOURCE_DIR}/external
)

target_link_libraries(pipeline
	PUBLIC
		${CMAKE_THREAD_LIBS_INIT}
)

if (USE_OPENCL)
  target_include_directories(pipeline SYSTEM
    PUBLIC
      ${OPENCL_INCLUDE_DIRS}
  )

  target_link_libraries(pipeline
    PUBLIC
    ${OPENCL_LIBRARIES}
  )

  target_compile_options(pipeline
    PUBLIC
      -DPIPELINE_ENABLE_OPENCL
  )
endif()

add_subdirectory("test")

add_subdirectory("examples")
