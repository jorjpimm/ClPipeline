#include <array>
#include <chrono>
#include <fstream>
#include <iostream>
#include <random>
#include <stdexcept>
#include <vector>

extern "C"
{
#define half float
#define __constant
#define __local
#define __global
#define __private
#include "cl/common.h"
}

#include "cl.h"

////////////////////////////////////////////////////////////////////////////////

std::string get_source(const char *file)
{
  auto filename = std::string(SOURCE_INCLUDE_DIR) + "/" + file;
  std::ifstream t(filename);
  if (!t.is_open())
    {
    throw std::runtime_error("Failed to open file");
    }

  std::string str((std::istreambuf_iterator<char>(t)),
                   std::istreambuf_iterator<char>());

  return str;
}

void dummy_data(short (&data)[ELEMENT_SIZE])
{
	std::random_device rd;

	std::mt19937 gen(rd());
	std::uniform_real_distribution<> dist1(5, 10);
	std::uniform_real_distribution<> dist2(15, 20);

	for (std::size_t i = 0; i < 100; ++i)
	{
		data[i] = (short)dist1(gen);
	}

	for (std::size_t i = 100; i < 200; ++i)
	{
		data[i] = (short)dist2(gen);
	}

	for (std::size_t i = 200; i < 300; ++i)
	{
		data[i] = (short)dist1(gen);
	}
}

////////////////////////////////////////////////////////////////////////////////
template <typename T> void time(const char *name, T t)
{
	auto count = 1;
	auto begin = std::chrono::high_resolution_clock::now();

	for (std::size_t j = 0; j < count; ++j)
	{
		t();
	}

	auto end = std::chrono::high_resolution_clock::now();
	auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
	std::cout << name << ": " << (float)ms/(float)count << "ms" << std::endl;
}

#if 0
int main(int argc, char** argv)
{
  auto stream_count = 12288;
  std::vector<Input> data(stream_count);
	for (auto &d : data)
	{
		dummy_data(d.val);
	}
  std::vector<Output> results(stream_count, Output{});

	auto bytes = data.size() * sizeof(Input);
	std::cout << "size: " << bytes << std::endl;

	/*time("cpu", [&](){
		for (std::size_t i = 0; i < data.size(); ++i)
		{
			event_detect(data[i].val, results[i].values, ELEMENT_SIZE);
		}
	});*/

	auto do_opencl = [&](const char *desc, cl::Device::DeviceType type)
	{
		auto device = cl::Device::get_default(type);
		auto context = device.create_context();

		auto command_queue = context.create_command_queue();

		auto src = get_source("kernel.cl");
		auto common = get_source("common.h");
		auto program = context.create_program({ common.c_str(), src.c_str() });

		auto kernel = program.create_kernel("run_kernel");

		// Create the input and output arrays in device memory for our calculation
		//
		auto input = context.create_buffer(CL_MEM_READ_ONLY, sizeof(data[0])*stream_count);
		auto output = context.create_buffer(CL_MEM_WRITE_ONLY, sizeof(results[0])*stream_count);
		auto mapping_in = command_queue.map(input, CL_TRUE, CL_MAP_WRITE, 0, sizeof(data[0])*stream_count, data.data());
		auto mapping_out = command_queue.map(output, CL_TRUE, CL_MAP_READ, 0, sizeof(results[0])*stream_count, results.data());

		Input *input_mapping = mapping_in.as<Input>();
		for (std::size_t i = 0; i < data.size(); ++i)
		{
			dummy_data(input_mapping[i].val);
		}

		time(desc, [&]() {
			kernel.set_kernel_arg(0, input);
			kernel.set_kernel_arg(1, output);
			kernel.set_kernel_arg(2, ELEMENT_SIZE);
      
      command_queue.execute(kernel, stream_count);
      command_queue.execute(kernel, stream_count);
      command_queue.execute(kernel, stream_count);
      command_queue.execute(kernel, stream_count);
      command_queue.execute(kernel, stream_count);
      command_queue.execute(kernel, stream_count);
      command_queue.execute(kernel, stream_count);
      command_queue.execute(kernel, stream_count);
      command_queue.finish();
		});
	};
  
  do_opencl("cl gpu", cl::Device::DeviceType::GPU);
	do_opencl("cl cpu", cl::Device::DeviceType::CPU);

  return 0;
}
#endif
