#include "pipeline/Queue/LocalQueue.h"
#include "pipeline/ObjectArray.h"
#include "pipeline/TypedObject.h"

#include <set>

#include <catch/catch.hpp>

struct LocalQueueTestArg
{
  std::size_t count = 0;
};

PIPELINE_DEFINE_TYPE(LocalQueueTestArg)
PIPELINE_IMPLEMENT_TYPE(LocalQueueTestArg)


struct LocalQueueDummyLayout
{
  struct Memory
  {
    LocalQueueTestArg args[2];
  } memory;

  struct MappingType
  {
  };

  using ObjectArrayType  = pipeline::ObjectArray;;

	ObjectArrayType get_object_array(
    pipeline::MemoryMapType type,
    const Memory &allocated,
    pipeline::ObjectLocation object) const
  {
    auto ref = pipeline::MemoryReference::initialise(
      const_cast<LocalQueueTestArg *>(&allocated.args[object.id()]),
      sizeof(LocalQueueTestArg)
    );
    return pipeline::ObjectArray::initialise(
      pipeline::TypeFinder<LocalQueueTestArg>::get(),
      ref,
      1,
      1, 
      0
    );
  }
};

SCENARIO("LocalQueue", "[object]")
{
  auto type = pipeline::TypeFinder<LocalQueueTestArg>::get();

  auto strat = pipeline::ObjectLocation::AllocationStrategy::PerStream;
  pipeline::ObjectLocation arg_1(0, type, strat);
  pipeline::ObjectLocation arg_2(1, type, strat);
  pipeline::LocalPerStreamComputation comp({ type }, { type },
    [](const pipeline::LocalPerStreamComputation::Arguments &args)
    {
      ++args.at<LocalQueueTestArg>(1)->count;
    }
  );
  pipeline::PipelineBuilder::PipelineCall call(&comp, { arg_1, arg_2 });

  std::set<pipeline::Queue::EnqueueResult> enqueued;
  std::set<pipeline::Queue::EnqueueResult> finished;
  pipeline::LocalQueue<LocalQueueDummyLayout> queue(
    [&finished](pipeline::Queue::EnqueueResult res)
    {
      finished.insert(res);
    }
  );

  WHEN("Queuing single work items")
  {
    LocalQueueDummyLayout memory_layout;
    REQUIRE(memory_layout.memory.args[0].count == 0);
    REQUIRE(memory_layout.memory.args[1].count == 0);
    enqueued.insert(queue.enqueue(call, 1, memory_layout, memory_layout.memory));
    REQUIRE(enqueued.size() == 1);

    queue.block_for_finish();
    REQUIRE(memory_layout.memory.args[0].count == 0);
    REQUIRE(memory_layout.memory.args[1].count == 1);
    REQUIRE(enqueued == finished);
  }

  WHEN("Queuing many work items")
  {
		const std::size_t enqueue_count = 30;
    LocalQueueDummyLayout memory_layout;

    REQUIRE(memory_layout.memory.args[0].count == 0);
    REQUIRE(memory_layout.memory.args[1].count == 0);
    for (std::size_t i = 0; i < enqueue_count; ++i)
    {
      enqueued.insert(queue.enqueue(call, 1, memory_layout, memory_layout.memory));
    }
    REQUIRE(enqueued.size() == enqueue_count);

    queue.block_for_finish();
    REQUIRE(memory_layout.memory.args[0].count == 0);
    REQUIRE(memory_layout.memory.args[1].count == enqueue_count);
    REQUIRE(enqueued == finished);

  }
}
