#include "pipeline/Object.h"
#include "pipeline/TypeFinder.h"

#include <catch/catch.hpp>

struct ObjectTest
{
  int a;
  float b;
};

PIPELINE_DEFINE_TYPE(ObjectTest)
PIPELINE_IMPLEMENT_TYPE(ObjectTest)

SCENARIO("Object", "[object]")
{
  GIVEN("a null object")
  {
    pipeline::Object obj;

    THEN("object is null")
    {
      REQUIRE(obj.get() == nullptr);
      REQUIRE(obj.type() == nullptr);
    }
  }

  GIVEN("an initialised object")
  {
    ObjectTest obj_data;
    auto memory = pipeline::MemoryReference::initialise(&obj_data, sizeof(obj_data));
    auto type = pipeline::TypeFinder<ObjectTest>::get();
    auto obj = pipeline::Object::initialise(memory, type);

    REQUIRE(obj.get() == &obj_data);
    REQUIRE(obj.type() == type);
  }
}
