#include "pipeline/Memory/LocalMemory.h"
#include "pipeline/TypeFinder.h"

#include <catch/catch.hpp>

struct LocalMemoryType
{
  int a;
  float b;
};

PIPELINE_DEFINE_TYPE(LocalMemoryType)
PIPELINE_IMPLEMENT_TYPE(LocalMemoryType)

SCENARIO("LocalMemory", "[memory][local]")
{
  GIVEN("memory created for description")
  {
    auto desc = pipeline::MemoryDescription::for_type<int>();
    auto mem1 = pipeline::LocalMemory::create(desc);
    auto mem2 = pipeline::LocalMemory::create(desc);

    THEN("allocation works correctly")
    {
      REQUIRE(mem1.map(pipeline::MemoryMapType::Read).value());
      REQUIRE(mem2.map(pipeline::MemoryMapType::Read).value());
      REQUIRE(mem1.map(pipeline::MemoryMapType::Read) != mem2.map(pipeline::MemoryMapType::Read));
    }
  }

  GIVEN("memory created for description")
  {
    auto mem = pipeline::LocalMemory::create(pipeline::TypeFinder<LocalMemoryType>::get());

    THEN("allocation worked correctly")
    {
      REQUIRE(mem.map(pipeline::MemoryMapType::Write).value());

      auto mapped = mem.map(pipeline::MemoryMapType::Read);

      auto a_mapped = mem.map(
				pipeline::MemoryMapType::Read,
        pipeline::MemoryDescription::MemoryOffset(
          offsetof(LocalMemoryType, a)),
          sizeof(LocalMemoryType::a));
      auto b_mapped = mem.map(
				pipeline::MemoryMapType::Read,
        pipeline::MemoryDescription::MemoryOffset(
          offsetof(LocalMemoryType, b)),
          sizeof(LocalMemoryType::b));

      REQUIRE(mapped == a_mapped);
      REQUIRE(mapped == static_cast<char*>(b_mapped.value()) - 4);
    }
  }
}
