#include "pipeline/Queue/ClQueue.h"

#include "pipeline/Computation/ClComputation.h"
#include "pipeline/TypedObject.h"

#include "ClQueueDummyLayout.h"

#include <set>
#include <catch/catch.hpp>

#ifdef PIPELINE_ENABLE_OPENCL

struct ClQueueTestArg
{
  int count;
};

PIPELINE_DEFINE_TYPE(ClQueueTestArg)
PIPELINE_IMPLEMENT_TYPE(ClQueueTestArg)

SCENARIO("ClQueue", "[object][cl]")
{
	auto device = cl::Device::get_default();
	cl::Context context = device.create_context();

  auto type = pipeline::TypeFinder<ClQueueTestArg>::get();
  
  auto strat = pipeline::ObjectLocation::AllocationStrategy::PerStream;
  pipeline::ObjectLocation arg_1(0, type, strat);
  pipeline::ObjectLocation arg_2(1, type, strat);
  pipeline::ClComputation comp(context, { type }, { type },
		"run_kernel",
		R"CL(
		__kernel void run_kernel(
      __global int* a,
      __global int* b)
  	{
  		++b[0];
  	}
  )CL");
  pipeline::PipelineBuilder::PipelineCall call(&comp, { arg_1, arg_2 });

  using MemoryLayout = ClQueueDummyLayout<ClQueueTestArg>;
  
  std::mutex finished_mutex;
  std::set<pipeline::Queue::EnqueueResult> enqueued;
  std::set<pipeline::Queue::EnqueueResult> finished;
	{
		pipeline::ClQueue<MemoryLayout> queue(
			context,
			[&finished, &finished_mutex](pipeline::Queue::EnqueueResult res)
			{
        std::lock_guard<std::mutex> l(finished_mutex);
				finished.insert(res);
			}
		);
  
		std::vector<pipeline::ObjectLocation> objects{
      pipeline::ObjectLocation(0, pipeline::TypeFinder<ClQueueTestArg>::get(), pipeline::ObjectLocation::AllocationStrategy::PerStream),
      pipeline::ObjectLocation(1, pipeline::TypeFinder<ClQueueTestArg>::get(), pipeline::ObjectLocation::AllocationStrategy::PerStream)
    };
  
		WHEN("Queuing single work items")
		{
			const std::size_t stream_count = 1;
			MemoryLayout memory_layout(queue.cl_queue(), objects, stream_count);
			MemoryLayout::Memory memory = memory_layout.create(context);
			memory_layout.set(memory, 0, 0);
			memory_layout.set(memory, 1, 0);

			memory_layout.check(memory, 0, 0);
			memory_layout.check(memory, 1, 0);
			enqueued.insert(queue.enqueue(call, stream_count, memory_layout, memory));
			REQUIRE(enqueued.size() == 1);

			queue.block_for_finish();
			memory_layout.check(memory, 0, 0);
			memory_layout.check(memory, 1, 1);
		}

		WHEN("Queuing many work items")
    {
    
			const std::size_t enqueue_count = 10;
			const std::size_t stream_count = 1;
			MemoryLayout memory_layout(queue.cl_queue(),objects, stream_count);
			MemoryLayout::Memory memory = memory_layout.create(context);
			memory_layout.set(memory, 0, 1);
			memory_layout.set(memory, 1, 0);

			memory_layout.check(memory, 0, 1);
			memory_layout.check(memory, 1, 0);
			for (std::size_t i = 0; i < enqueue_count; ++i)
			{
				enqueued.insert(queue.enqueue(call, stream_count, memory_layout, memory));
			}
			REQUIRE(enqueued.size() == enqueue_count);

			queue.block_for_finish();
			memory_layout.check(memory, 0, 1);
			memory_layout.check(memory, 1, enqueue_count);
		}
	}
  REQUIRE(enqueued == finished);
}

#endif
