#include "pipeline/ObjectArray.h"

#include <catch/catch.hpp>

SCENARIO("ObjectArray", "[object]")
{
  auto type = pipeline::Type::create<int>("int");

  GIVEN("a null object array")
  {
    auto array = pipeline::ObjectArray();
    REQUIRE(array.size() == 0);
    REQUIRE(array.begin() == array.end());
  }

  GIVEN("an object array with normal stride")
  {
    int data[15];
    auto ref = pipeline::MemoryReference::initialise(data, sizeof(data));
    auto array = pipeline::ObjectArray::initialise(
      &type,
      ref,
      15,
      15,
      sizeof(data[0])
    );

    REQUIRE(array.size() == 15);
    REQUIRE(array.begin() + 15 == array.end());
    std::size_t index = 0;
    for (auto e : array)
    {
      REQUIRE(e.get() == data + index);
      index++;
    }
  }

  GIVEN("an object array with zero stride")
  {
    int data[1];
    auto ref = pipeline::MemoryReference::initialise(data, sizeof(data));
    auto array = pipeline::ObjectArray::initialise(
      &type,
      ref,
      15,
      1,
      0
    );

    REQUIRE(array.size() == 1);
    REQUIRE(array.at_stream(0).get() == array.at_stream(14).get());
  
    std::size_t count = 0;
    for (auto e : array)
    {
      REQUIRE(e.get() == data);
      ++count;
    }
    REQUIRE(count == 1);
  }
}
