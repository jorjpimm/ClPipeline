#include "pipeline/Computation/Computation.h"

#include <catch/catch.hpp>

SCENARIO("Computation", "[computation]")
{
  auto typeA = pipeline::Type::create<int>("TestA");
  auto typeB = pipeline::Type::create<float>("TestB");
  auto typeC = pipeline::Type::create<double>("TestC");

  GIVEN("a computation object")
  {
    pipeline::Computation comp(
      { &typeB, &typeA },
      { &typeC },
      pipeline::Computation::EvaluationStrategy::PerStream,
      pipeline::Computation::ComputationType::Unknown);

    THEN("accessors are correct")
    {
      auto exp_inputs = std::vector<const pipeline::Type *>{ &typeB, &typeA };
      auto exp_outputs = std::vector<pipeline::Computation::Output>{ &typeC };
      REQUIRE(comp.inputs() == exp_inputs);
      REQUIRE(comp.outputs() == exp_outputs);
      REQUIRE(comp.type() == pipeline::Computation::ComputationType::Unknown);
    }
  }
}
