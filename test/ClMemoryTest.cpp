#include "pipeline/Memory/ClMemory.h"
#include "pipeline/TypeFinder.h"

#include <catch/catch.hpp>

#ifdef PIPELINE_ENABLE_OPENCL

struct ClMemoryType
{
  int a;
  float b;
};

PIPELINE_DEFINE_TYPE(ClMemoryType)
PIPELINE_IMPLEMENT_TYPE(ClMemoryType)

SCENARIO("ClMemory", "[memory][cl]")
{
  auto device = cl::Device::get_default();
  auto context = device.create_context();
  auto queue = context.create_command_queue();

  GIVEN("memory created for description")
  {
    auto desc = pipeline::MemoryDescription::for_type<int>();

    auto mem1 = pipeline::ClMemory::initialise(
      context.create_buffer(CL_MEM_READ_WRITE, desc.size()), desc.size(), &queue);
    auto mem2 = pipeline::ClMemory::initialise(
      context.create_buffer(CL_MEM_READ_WRITE, desc.size()), desc.size(), &queue);

    THEN("allocation works correctly")
    {
      auto mem1_mapped = mem1.map(pipeline::MemoryMapType::Read);
      auto mem2_mapped = mem2.map(pipeline::MemoryMapType::Read);
      REQUIRE(mem1_mapped.value());
      REQUIRE(mem1_mapped.value());
      REQUIRE(mem1_mapped != mem2_mapped);
    }
  }

  GIVEN("memory created for description")
  {
    auto desc = pipeline::TypeFinder<ClMemoryType>::get();
    auto mem = pipeline::ClMemory::initialise(
      context.create_buffer(
        CL_MEM_READ_WRITE,
        desc->memory_description().size()
      ),
      desc->memory_description().size(),
      &queue
    );

    THEN("allocation worked correctly")
    {
      REQUIRE(mem.map(pipeline::MemoryMapType::Read).value());

			{
				auto mapped = mem.map(pipeline::MemoryMapType::Write);

				auto mapped_val = static_cast<ClMemoryType *>(mapped.value());
				mapped_val->a = 4;
				mapped_val->b = 5.0f;
			}

      auto a_mapped = mem.map(
				pipeline::MemoryMapType::Read,
        pipeline::MemoryDescription::MemoryOffset(
          offsetof(ClMemoryType, a)),
          sizeof(ClMemoryType::a));
      auto b_mapped = mem.map(
				pipeline::MemoryMapType::Read,
        pipeline::MemoryDescription::MemoryOffset(
          offsetof(ClMemoryType, b)),
          sizeof(ClMemoryType::b));

      auto a_mapped_val = static_cast<int *>(a_mapped.value());
      auto b_mapped_val = static_cast<float *>(b_mapped.value());
    
      REQUIRE(4 == *a_mapped_val);
      REQUIRE(5.0f == *b_mapped_val);
    }
  }
}

#endif