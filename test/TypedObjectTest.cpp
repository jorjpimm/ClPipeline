#include "pipeline/TypedObject.h"
#include "pipeline/TypeFinder.h"

#include <catch/catch.hpp>

struct TypedObjectTest
{
  int a;
  float b;
};

PIPELINE_DEFINE_TYPE(TypedObjectTest)
PIPELINE_IMPLEMENT_TYPE(TypedObjectTest)

SCENARIO("TypedObject", "[object]")
{
  GIVEN("an typed object class")
  {
    REQUIRE(pipeline::TypedObject<TypedObjectTest>::static_type() ==
      pipeline::TypeFinder<TypedObjectTest>::get());
  }

  GIVEN("an initialised object")
  {
    TypedObjectTest obj_data;
    obj_data.a = 5;
    obj_data.b = 6.0f;
    auto memory = pipeline::MemoryReference::initialise(&obj_data, sizeof(obj_data));
    auto obj = pipeline::TypedObject<TypedObjectTest>::initialise(memory);

    REQUIRE(&obj.get() == &obj_data);
    REQUIRE(obj.type() == pipeline::TypeFinder<TypedObjectTest>::get());

    REQUIRE(obj->a == obj_data.a);
    REQUIRE(obj->b == obj_data.b);
  }

  GIVEN("an cast object")
  {
    TypedObjectTest obj_data;
    auto memory = pipeline::MemoryReference::initialise(&obj_data, sizeof(obj_data));
    auto type = pipeline::TypeFinder<TypedObjectTest>::get();
    auto base_obj = pipeline::Object::initialise(memory, type);
    auto &obj = pipeline::TypedObject<TypedObjectTest>::cast(base_obj);

    REQUIRE(&obj == &base_obj);
    REQUIRE(&obj.get() == &obj_data);
    REQUIRE(obj.type() == type);
  }

  GIVEN("from an object")
  {
    TypedObjectTest obj_data;
    auto memory = pipeline::MemoryReference::initialise(&obj_data, sizeof(obj_data));
    auto type = pipeline::TypeFinder<TypedObjectTest>::get();
    auto base_obj = pipeline::Object::initialise(memory, type);
    auto obj = pipeline::TypedObject<TypedObjectTest>::from(base_obj);

    REQUIRE(&obj.get() == &obj_data);
    REQUIRE(obj.type() == type);
  }
}
