#include "pipeline/Pipeline.h"
#include "pipeline/Queue/Queue.h"
#include "pipeline/TypeFinder.h"

#include <catch/catch.hpp>

SCENARIO("Pipeline", "[pipeline]")
{
  const std::size_t stream_count = 1;

  struct Allocator
  {
  };

  struct Layout
  {
    struct Memory
    {
    };

    struct ObjectArrayType
    {
    };

    Layout(
      const gsl::span<const pipeline::ObjectLocation> &in_objects,
      std::size_t in_stream_count)
    : objects(in_objects.begin(), in_objects.end())
    , stream_count(in_stream_count)
    {
    }

    Memory create(Allocator &alloc)
    {
      return Memory{};
    }

    std::vector<pipeline::ObjectLocation> objects;
    std::size_t stream_count;
  };

  struct Queue : public pipeline::Queue
  {
    struct Enqueued
    {
    		const pipeline::PipelineBuilder::PipelineCall *call;
    		std::size_t stream_count;
    		const Layout *layout;
    		typename Layout::Memory *memory;
    };

    EnqueueResult enqueue(
      const pipeline::PipelineBuilder::PipelineCall &call,
  		std::size_t stream_count,
  		const Layout &layout,
  		typename Layout::Memory &memory)
  	{
      EnqueueResult id = work_queue.size();
      work_queue.emplace_back(Enqueued{&call, stream_count, &layout, &memory});
      return id;
    }

    void block_for_finish()
    {
    }

    std::vector<Enqueued> work_queue;
  };


  GIVEN("an empty pipeline")
  {
    Allocator allocator;
    Queue queue;

    pipeline::PipelineBuilder builder;
    pipeline::Pipeline<Layout, Queue> pipeline(builder, allocator, queue, stream_count);

    THEN("no queueing occurs")
    {
      std::vector<pipeline::ObjectLocation> empty_objects;
      REQUIRE(pipeline.layout().stream_count == stream_count);
      REQUIRE(pipeline.layout().objects == empty_objects);

      pipeline.run();
      REQUIRE(queue.work_queue.empty());
    }
  }

  GIVEN("an simple pipeline")
  {
    Allocator allocator;
    Queue queue;

    auto float_type = pipeline::Type::create<float>("float");
    auto int_type = pipeline::Type::create<int>("int");

    pipeline::Computation comp(
      { &float_type, &int_type },
      { &int_type },
      pipeline::Computation::EvaluationStrategy::PerStream,
      pipeline::Computation::ComputationType::Unknown
    );

    pipeline::PipelineBuilder builder;
    auto strat = pipeline::ObjectLocation::AllocationStrategy::PerStream;
    auto arg_0 = builder.add_argument(&float_type, strat);
    auto arg_1 = builder.add_argument(&int_type, strat);
    std::vector<pipeline::ObjectLocation> outputs;
    auto pipe_call = builder.add_call(
			comp,
      { arg_0, arg_1 },
			outputs
		);
    pipeline::Pipeline<Layout, Queue> pipeline(builder, allocator, queue, stream_count);

    THEN("no queueing occurs")
    {
      REQUIRE(pipeline.layout().stream_count == stream_count);
      std::vector<pipeline::ObjectLocation> expected_objects;
      expected_objects.push_back(pipeline::ObjectLocation(0, &float_type, strat));
      expected_objects.push_back(pipeline::ObjectLocation(1, &int_type, strat));
      expected_objects.push_back(pipeline::ObjectLocation(2, &int_type, strat));

      REQUIRE(pipeline.layout().objects == expected_objects);

      pipeline.run();
      REQUIRE(queue.work_queue.size() == 1);
    }
  }
}
