#pragma once

#ifdef PIPELINE_ENABLE_OPENCL

#include "pipeline/Memory/ClMemory.h"
#include "pipeline/Memory/Layout/SOAMemoryLayout.h"
#include "pipeline/TypeFinder.h"
#include <catch/catch.hpp>

template <typename T> struct ClQueueDummyLayout : pipeline::SOAMemoryLayout<pipeline::ClMemory>
{
  ClQueueDummyLayout(
    cl::CommandQueue &queue,
    const gsl::span<const pipeline::ObjectLocation> &objects,
    std::size_t stream_count)
  : pipeline::SOAMemoryLayout<pipeline::ClMemory>(objects, stream_count)
	, m_queue(queue)
  {
  }

	Memory create(cl::Context &ctx)
	{
		struct
		{
			pipeline::ClMemory allocate(const pipeline::MemoryDescription &desc)
			{
				return pipeline::ClMemory::initialise(ctx->create_buffer(CL_MEM_READ_WRITE, desc.size()), desc.size(), queue);
			}

			cl::CommandQueue *queue;
			cl::Context *ctx;
		} allocator;
		allocator.ctx = &ctx;
		allocator.queue = &m_queue;
		return  pipeline::SOAMemoryLayout<pipeline::ClMemory>::create(allocator);
	}

	void check(Memory &mem, std::size_t index, int expected)
	{
		auto mapped = m_queue.map(mem.objects[index].cl(), CL_TRUE, CL_MEM_READ_ONLY, 0, sizeof(T));
		auto data = mapped.template as<T>();
		REQUIRE(data->count == expected);
	}

	void set(Memory &mem, std::size_t index, int value)
	{
		auto mapped = m_queue.map(mem.objects[index].cl(), CL_TRUE, CL_MEM_WRITE_ONLY, 0, sizeof(T));
		auto data = mapped.template as<T>();
		data->count = value;
	}

private:
  cl::CommandQueue &m_queue;
};

#endif
