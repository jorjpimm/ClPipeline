#include "pipeline/Computation/ClComputation.h"

#include <catch/catch.hpp>

#ifdef PIPELINE_ENABLE_OPENCL

SCENARIO("ClComputation", "[computation][cl]")
{
  auto int_type = pipeline::Type::create<int>("int");
  auto float_type = pipeline::Type::create<float>("float");
  auto short_type = pipeline::Type::create<short>("short");

  GIVEN("a computation object")
  {
    auto device = cl::Device::get_default();
    cl::Context context = device.create_context();

    pipeline::ClComputation comp(
      context,
      { &int_type, &float_type },
      { &short_type },
      "run_kernel",
      R"CL(

  __kernel void run_kernel(
     __global int* a,
     __global float* b,
  	 __global short* c)
  	{
  		c[0] = a[0] + b[0];
  	}
  )CL");

    THEN("Computation is executed correctly")
    {
      auto int_cl = context.create_buffer(CL_MEM_READ_WRITE, int_type.memory_description().size());
      auto float_cl = context.create_buffer(CL_MEM_READ_WRITE, float_type.memory_description().size());
      auto short_cl = context.create_buffer(CL_MEM_READ_WRITE, short_type.memory_description().size());
      auto queue = context.create_command_queue();

      {
        auto mapped = queue.map(int_cl, CL_TRUE, CL_MAP_WRITE, 0, int_type.memory_description().size());
        *static_cast<int *>(mapped.get()) = 4;
      }
      {
        auto mapped = queue.map(float_cl, CL_TRUE, CL_MAP_WRITE, 0, float_type.memory_description().size());
        *static_cast<float *>(mapped.get()) = 5.0;
      }

      comp.kernel().set_kernel_arg(0, int_cl);
      comp.kernel().set_kernel_arg(1, float_cl);
      comp.kernel().set_kernel_arg(2, short_cl);

      queue.execute(comp.kernel(), 1);

      queue.finish();

      {
        auto mapped = queue.map(short_cl, CL_TRUE, CL_MAP_READ, 0, short_type.memory_description().size());
        REQUIRE(*static_cast<short *>(mapped.get()) == 9);
      }
    }
  }
}

#endif