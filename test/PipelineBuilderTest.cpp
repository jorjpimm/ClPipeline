#include "pipeline/PipelineBuilder.h"
#include "pipeline/Type.h"

#include <catch/catch.hpp>

SCENARIO("PipelineBuilder", "[pipeline]")
{
  auto float_type = pipeline::Type::create<float>("float");
  auto int_type = pipeline::Type::create<int>("int");

  GIVEN("a pipeline with arguments")
  {
    auto pipe = pipeline::PipelineBuilder();

    auto arg_1 = pipe.add_argument(&float_type, pipeline::ObjectLocation::AllocationStrategy::PerStream);
    auto arg_2 = pipe.add_argument(&int_type, pipeline::ObjectLocation::AllocationStrategy::PerStream);


		THEN("arguments are correct")
		{
			std::vector<pipeline::ObjectLocation> exp_args = { arg_1, arg_2 };
      std::vector<pipeline::ObjectLocation> exp_outputs = {};
			REQUIRE(pipe.objects() == exp_args);
			REQUIRE(pipe.arguments() == exp_args);
			REQUIRE(pipe.outputs() == exp_outputs);
		}

		auto output = pipe.add_output(arg_2);

		THEN("outputs are correct")
		{
			REQUIRE(output == arg_2);
			std::vector<pipeline::ObjectLocation> exp_args = { arg_1, arg_2 };
			std::vector<pipeline::ObjectLocation> exp_outputs = { arg_2 };
			REQUIRE(pipe.objects() == exp_args);
			REQUIRE(pipe.arguments() == exp_args);
			REQUIRE(pipe.outputs() == exp_outputs);
		}

		pipeline::Computation call(
			{ &float_type, &int_type },
			{ &int_type },
      pipeline::Computation::EvaluationStrategy::PerStream,
			pipeline::Computation::ComputationType::Unknown
		);

		THEN("adding incorrect arguments count fails")
		{
			std::vector<pipeline::ObjectLocation> outputs;
			REQUIRE_THROWS(pipe.add_call(
				call,
				std::vector<pipeline::ObjectLocation>{arg_1},
				outputs
			));
		}

		THEN("adding incorrect arguments fails")
		{
			std::vector<pipeline::ObjectLocation> outputs;
			REQUIRE_THROWS(pipe.add_call(
				call,
				std::vector<pipeline::ObjectLocation>{arg_2, arg_1},
				outputs
			));
		}

		auto call_args = std::vector<pipeline::ObjectLocation>{ arg_1, arg_2 };
		std::vector<pipeline::ObjectLocation> outputs;
		auto pipe_call = pipe.add_call(
			call,
			call_args,
			outputs
		);

		auto function_call_args = std::vector<pipeline::ObjectLocation>{ arg_1, arg_2, outputs[0] };
		THEN("adding arguments gives correct outputs")
		{
			REQUIRE(pipe_call);
			REQUIRE(outputs.size() == 1);
			REQUIRE(pipe_call->arguments() == function_call_args);
		}

		auto output_arg = pipe.add_output(outputs[0]);

		THEN("outputs are correct")
		{
			REQUIRE(output == arg_2);
			std::vector<pipeline::ObjectLocation> exp_args = { arg_1, arg_2 };
			std::vector<pipeline::ObjectLocation> exp_outputs = { arg_2, output_arg };
			REQUIRE(pipe.objects() == function_call_args);
			REQUIRE(pipe.arguments() == exp_args);
			REQUIRE(pipe.outputs() == exp_outputs);
		}
  }
}
