#include "pipeline/Type.h"

#include <catch/catch.hpp>

SCENARIO("Type", "[memory][type]")
{
  GIVEN("A basic type")
  {
    const char *name = "TestType";
    struct TestType
    {
      int a;
      float b;
    };

    auto type = pipeline::Type::create<TestType>(name);
    THEN("Accesors are correct")
    {
      REQUIRE(type.name() == name);
      REQUIRE(type.is_trivial() == true);
      REQUIRE(type.memory_description() == pipeline::MemoryDescription::for_type<TestType>());
    }

    THEN("Creating types work")
    {
      TestType orig = { 3, 5.4f };
      std::aligned_storage<sizeof(TestType), alignof(TestType)>::type storage;
      TestType *data = reinterpret_cast<TestType *>(&storage);
      *data = orig;
      REQUIRE(data->a == orig.a);
      REQUIRE(data->b == orig.b);

      auto ref = pipeline::MemoryReference::initialise(data, sizeof(storage));
      type.emplace(ref);
      REQUIRE(data->a == orig.a);
      REQUIRE(data->b == orig.b);

      type.destroy(ref);
      REQUIRE(data->a == orig.a);
      REQUIRE(data->b == orig.b);
    }
  }

  GIVEN("A non trivial type")
  {
    const char *name = "TestType";
    struct TestType
    {
      TestType()
      {
        a = 3;
        b = 5.4f;
      }
      int a;
      float b;
    };

    auto type = pipeline::Type::create<TestType>(name);

    THEN("Accesors are correct")
    {
      REQUIRE(type.name() == name);
      REQUIRE(type.is_trivial() == false);
      REQUIRE(type.memory_description() == pipeline::MemoryDescription::for_type<TestType>());
    }

    THEN("Creating types work")
    {
      TestType orig;
      std::aligned_storage<sizeof(TestType), alignof(TestType)>::type storage;
      TestType *data = reinterpret_cast<TestType *>(&storage);

      auto ref = pipeline::MemoryReference::initialise(data, sizeof(storage));
      type.emplace(ref);
      REQUIRE(data->a == orig.a);
      REQUIRE(data->b == orig.b);

      type.destroy(ref);
      REQUIRE(data->a == orig.a);
      REQUIRE(data->b == orig.b);
    }
  }
}
