#include "pipeline/Memory/MemoryDescription.h"
#include "utils.h"

#include <catch/catch.hpp>

SCENARIO("MemoryDescription", "[memory]")
{
  GIVEN("An empty description")
  {
    auto desc = pipeline::MemoryDescription();
    REQUIRE(desc.size() == 0);
    REQUIRE(desc.alignment() == 4);
  }

  GIVEN("A description from a simple type")
  {
    auto desc = pipeline::MemoryDescription::for_type<int>();

    THEN("The type is correctly sized")
    {
      REQUIRE(desc.size() == sizeof(int));
      REQUIRE(desc.alignment() == std::alignment_of<int>::value);
    }

    THEN("Aligning the type works correctly")
    {
      desc.align_size_to(2);
      REQUIRE(desc.size() == sizeof(int));

      desc.align_size_to(6);
      REQUIRE(desc.size() == 6);
    }
  }

  GIVEN("A description from struct type")
  {
    struct Complex
    {
      int a;
      float b;
    };

    auto desc = pipeline::MemoryDescription::for_type<Complex>();

    THEN("The type is correctly sized")
    {
      REQUIRE(desc.size() == sizeof(Complex));
      REQUIRE(desc.alignment() == std::alignment_of<Complex>::value);
    }
  }

  GIVEN("A description from struct type")
  {
    ALIGN_PREFIX(8) struct Aligned
    {
      int a;
      float b;
    } ALIGN_SUFFIX(8);

    auto desc = pipeline::MemoryDescription::for_type<Aligned>();

    THEN("The type is correctly sized")
    {
      REQUIRE(desc.size() == sizeof(Aligned));
      REQUIRE(desc.alignment() == 8);
    }
  }

  GIVEN("A description built from other types")
  {
    std::size_t expected_size = 0;
    auto desc = pipeline::MemoryDescription();

    REQUIRE(desc.add(pipeline::MemoryDescription::for_type<int>()) ==
            pipeline::MemoryDescription::MemoryOffset(expected_size));
    expected_size += sizeof(int);
    REQUIRE(desc.size() == expected_size);
    REQUIRE(desc.alignment() == std::alignment_of<int>::value);

    REQUIRE(desc.add(pipeline::MemoryDescription::for_type<float>()) ==
            pipeline::MemoryDescription::MemoryOffset(expected_size));
    expected_size += sizeof(float);
    REQUIRE(desc.size() == expected_size);
    REQUIRE(desc.alignment() == std::alignment_of<int>::value);

    REQUIRE(desc.add(pipeline::MemoryDescription::for_type<char>()) ==
            pipeline::MemoryDescription::MemoryOffset(expected_size));
    expected_size += sizeof(char);
    REQUIRE(desc.size() == expected_size);
    REQUIRE(desc.alignment() == std::alignment_of<int>::value);

		ALIGN_PREFIX(8) struct Aligned
    {
      int a;
      float b;
    } ALIGN_SUFFIX(8);

    expected_size += 7; // Rounding off for alignment
    REQUIRE(desc.add(pipeline::MemoryDescription::for_type<Aligned>()) ==
            pipeline::MemoryDescription::MemoryOffset(expected_size));
    expected_size += sizeof(Aligned);
    REQUIRE(desc.size() == expected_size);
    REQUIRE(desc.alignment() == std::alignment_of<Aligned>::value);
  }

  GIVEN("A description for arrays")
  {
		ALIGN_PREFIX(8) struct AlignedSmall
    {
      int a;
    } ALIGN_SUFFIX(8);

    auto type = pipeline::MemoryDescription::for_type<AlignedSmall>();

    THEN("The type is correctly sized")
    {
      auto desc = pipeline::MemoryDescription::array_of_n(type, 1);
      REQUIRE(desc.size() == 8);
      REQUIRE(desc.alignment() == 8);
    }

    THEN("The type is correctly sized")
    {
      auto desc = pipeline::MemoryDescription::array_of_n(type, 2);
      REQUIRE(desc.size() == 16);
      REQUIRE(desc.alignment() == 8);
    }
  }
}
