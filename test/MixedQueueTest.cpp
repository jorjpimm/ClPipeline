#include "pipeline/Queue/MixedQueue.h"

#include "pipeline/Computation/ClComputation.h"
#include "pipeline/Computation/LocalComputation.h"
#include "pipeline/TypedObject.h"

#include "ClQueueDummyLayout.h"

#include <set>

#include <catch/catch.hpp>

#ifdef PIPELINE_ENABLE_OPENCL

struct MixedQueueTestArg
{
  int count;
};

PIPELINE_DEFINE_TYPE(MixedQueueTestArg)
PIPELINE_IMPLEMENT_TYPE(MixedQueueTestArg)

SCENARIO("MixedQueue", "[object][cl]")
{
  auto device = cl::Device::get_default();
  cl::Context context = device.create_context();

  auto type = pipeline::TypeFinder<MixedQueueTestArg>::get();
  
  auto strat = pipeline::ObjectLocation::AllocationStrategy::PerStream;
  pipeline::ObjectLocation arg_1(0, type, strat);
  pipeline::ObjectLocation arg_2(1, type, strat);
  pipeline::ClComputation comp1(context, { type }, { type },
    "run_kernel",
    R"CL(
    __kernel void run_kernel(
      __global int* a,
      __global int* b)
    {
      ++b[0];
    }
  )CL");
  pipeline::LocalPerStreamComputation comp2({ type }, { type },
    [](const pipeline::LocalPerStreamComputation::Arguments &args)
    {
      ++args.at<MixedQueueTestArg>(1)->count;
    }
  );
  pipeline::PipelineBuilder::PipelineCall call1(&comp1, { arg_1, arg_2 });
  pipeline::PipelineBuilder::PipelineCall call2(&comp2, { arg_1, arg_2 });

  using MemoryLayout = ClQueueDummyLayout<MixedQueueTestArg>;

  std::set<pipeline::Queue::EnqueueResult> enqueued;
  std::set<pipeline::Queue::EnqueueResult> finished;
  {
		pipeline::MixedQueue<MemoryLayout> queue(
      context,
      [&finished](pipeline::Queue::EnqueueResult res)
      {
        finished.insert(res);
      }
    );
  
		std::vector<pipeline::ObjectLocation> objects{
      pipeline::ObjectLocation(0, pipeline::TypeFinder<MixedQueueTestArg>::get(), pipeline::ObjectLocation::AllocationStrategy::PerStream),
      pipeline::ObjectLocation(1, pipeline::TypeFinder<MixedQueueTestArg>::get(), pipeline::ObjectLocation::AllocationStrategy::PerStream)
    };
  
		WHEN("Queuing single work items")
    {
      auto &cl_queue = queue.cl_queue().cl_queue();

      const std::size_t stream_count = 1;
      MemoryLayout memory_layout(cl_queue, objects, stream_count);
      MemoryLayout::Memory memory = memory_layout.create(context);
      memory_layout.set(memory, 0, 0);
      memory_layout.set(memory, 1, 0);

      memory_layout.check(memory, 0, 0);
      memory_layout.check(memory, 1, 0);
      enqueued.insert(queue.enqueue(call1, stream_count, memory_layout, memory));
      enqueued.insert(queue.enqueue(call2, stream_count, memory_layout, memory));
      REQUIRE(enqueued.size() == 2);

      queue.block_for_finish();
      memory_layout.check(memory, 0, 0);
      memory_layout.check(memory, 1, 2);
    }

		WHEN("Queuing many work items")
    {
			const std::size_t enqueue_count = 30;

      auto &cl_queue = queue.cl_queue().cl_queue();

      const std::size_t stream_count = 1;
      MemoryLayout memory_layout(cl_queue, objects, stream_count);
      MemoryLayout::Memory memory = memory_layout.create(context);
      memory_layout.set(memory, 0, 0);
      memory_layout.set(memory, 1, 0);

      memory_layout.check(memory, 0, 0);
      memory_layout.check(memory, 1, 0);
      for (std::size_t i = 0; i < enqueue_count; ++i)
      {
        enqueued.insert(queue.enqueue(call1, stream_count, memory_layout, memory));
        enqueued.insert(queue.enqueue(call2, stream_count, memory_layout, memory));
      }
      REQUIRE(enqueued.size() == enqueue_count * 2);

      queue.block_for_finish();
      memory_layout.check(memory, 0, 0);
      memory_layout.check(memory, 1, enqueue_count * 2);
    }
  }
  REQUIRE(enqueued == finished);
}

#endif
