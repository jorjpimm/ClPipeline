#include "pipeline/ObjectLocation.h"
#include "pipeline/Type.h"

#include <catch/catch.hpp>

SCENARIO("ObjectLocation", "[pipeline][object]")
{
  GIVEN("a null pipeline object")
  {
    auto obj = pipeline::ObjectLocation();

    THEN("object is null")
    {
      REQUIRE(obj.type() == nullptr);
      REQUIRE(obj.id() == std::numeric_limits<pipeline::ObjectLocation::Id>::max());
      REQUIRE(obj.is_per_stream() == true);
    }
  }

  GIVEN("an object created from id and type, per stream")
  {
		auto type = pipeline::Type::create<int>("int");
    auto obj = pipeline::ObjectLocation(5, &type, pipeline::ObjectLocation::AllocationStrategy::PerStream);

    THEN("type refers to correct objects")
    {
      REQUIRE(obj.type() == &type);
      REQUIRE(obj.id() == 5);
      REQUIRE(obj.is_per_stream() == true);
    }
  }

  GIVEN("an object created from id and type, singular")
  {
		auto type = pipeline::Type::create<int>("int");
    auto obj = pipeline::ObjectLocation(5, &type, pipeline::ObjectLocation::AllocationStrategy::Single);

    THEN("type refers to correct objects")
    {
      REQUIRE(obj.type() == &type);
      REQUIRE(obj.id() == 5);
      REQUIRE(obj.is_per_stream() == false);
    }
  }
}
