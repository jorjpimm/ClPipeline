#include "pipeline/Memory/Layout/AOSMemoryLayout.h"
#include "pipeline/Memory/Layout/SOAMemoryLayout.h"
#include "pipeline/Memory/Memory.h"

#include <catch/catch.hpp>

struct DummyMemory
{
	using Storage = std::array<char, 256>;
	mutable Storage storage;

  struct MappedType
  {
		Storage *storage;
    pipeline::MemoryMapType map_type;
    pipeline::MemoryDescription::MemoryOffset offset;
    std::size_t size;

    operator void *()
    {
      return storage->data() + offset.offset();
    }

		void *base()
		{
			return storage->data();
		}
  };

  MappedType map(
    pipeline::MemoryMapType type,
    const pipeline::MemoryDescription::MemoryOffset &offset = pipeline::MemoryDescription::MemoryOffset(),
    std::size_t size = std::numeric_limits<std::size_t>::max()) const
  {
		return MappedType{ &storage, type, offset, size };
  }
};

struct LayoutTypeA
{
  LayoutTypeA()
  {
    ++ctor_count;
  }
  LayoutTypeA(const LayoutTypeA &)
  {
    ++copy_count;
  }
  ~LayoutTypeA()
  {
    ++dtor_count;
  }
  LayoutTypeA &operator=(const LayoutTypeA &)
  {
    ++copy_count;
    return *this;
  }

  int data;

  static void reset()
  {
    ctor_count = 0;
    copy_count = 0;
    dtor_count = 0;
  }

  static std::size_t ctor_count;
  static std::size_t copy_count;
  static std::size_t dtor_count;
};
std::size_t LayoutTypeA::ctor_count = 0;
std::size_t LayoutTypeA::copy_count = 0;
std::size_t LayoutTypeA::dtor_count = 0;


struct LayoutTypeB
{
  double data;
};

PIPELINE_DEFINE_TYPE(LayoutTypeA)
PIPELINE_IMPLEMENT_TYPE(LayoutTypeA)
PIPELINE_DEFINE_TYPE(LayoutTypeB)
PIPELINE_IMPLEMENT_TYPE(LayoutTypeB)

struct TestObjectLayout
{
  std::vector<pipeline::ObjectLocation> objects;

  static const std::size_t expected_single_size;
  static const std::size_t expected_stream_size;
  static const std::size_t expected_stream_padding;
  static const std::size_t expected_alignment;

  static std::size_t expected_size(std::size_t stream_count)
  {
    return (expected_stream_size + expected_stream_padding) * stream_count + expected_single_size;
  }

  TestObjectLayout()
  {
    REQUIRE(!pipeline::TypeFinder<LayoutTypeA>::get()->is_trivial());
    REQUIRE(pipeline::TypeFinder<LayoutTypeB>::get()->is_trivial());

    auto per_stream = pipeline::ObjectLocation::AllocationStrategy::PerStream;
    auto singular = pipeline::ObjectLocation::AllocationStrategy::Single;
    objects.emplace_back(0, pipeline::TypeFinder<LayoutTypeA>::get(), per_stream);
    objects.emplace_back(1, pipeline::TypeFinder<LayoutTypeB>::get(), per_stream);
    objects.emplace_back(2, pipeline::TypeFinder<LayoutTypeA>::get(), per_stream);
    objects.emplace_back(3, pipeline::TypeFinder<LayoutTypeB>::get(), per_stream);
    objects.emplace_back(4, pipeline::TypeFinder<LayoutTypeA>::get(), singular);
  }
};

const std::size_t TestObjectLayout::expected_stream_size = sizeof(LayoutTypeA) +
  sizeof(LayoutTypeB) +
  sizeof(LayoutTypeA) +
  sizeof(LayoutTypeB);

const std::size_t TestObjectLayout::expected_single_size = sizeof(LayoutTypeA);

const std::size_t TestObjectLayout::expected_stream_padding = 8;

const std::size_t TestObjectLayout::expected_alignment = std::max(std::alignment_of<LayoutTypeA>::value, std::alignment_of<LayoutTypeB>::value);

SCENARIO("AOSMemoryLayout", "[memory]")
{
  GIVEN("Test object layout")
  {
    static const std::size_t stream_count = 4;
    TestObjectLayout object_layout;
    pipeline::AOSMemoryLayout<DummyMemory> layout{ object_layout.objects, stream_count };

    WHEN("Creating memory from layout")
    {
      struct {
        DummyMemory allocate(const pipeline::MemoryDescription &desc)
        {
          REQUIRE(desc.alignment() == TestObjectLayout::expected_alignment);
          REQUIRE(desc.size() == TestObjectLayout::expected_size(stream_count));
					assert(desc.size() < sizeof(DummyMemory::storage));
          ++count;
          return DummyMemory{};
        }

        std::size_t count = 0;
      } test_allocator;

      {
        LayoutTypeA::reset();
        auto allocated = layout.create(test_allocator);

        THEN("constructors ae called correctly")
        {
          REQUIRE(LayoutTypeA::ctor_count == 9);
          REQUIRE(LayoutTypeA::copy_count == 0);
          REQUIRE(LayoutTypeA::dtor_count == 0);
        }

        THEN("Allocation routine is called correctly")
        {
          REQUIRE(test_allocator.count == 1);
        }

        THEN("objects are accessed correctly")
        {
  				auto obj0 = layout.get_object_array(pipeline::MemoryMapType::Read, allocated, object_layout.objects[0]);
  				auto obj1 = layout.get_object_array(pipeline::MemoryMapType::Read, allocated, object_layout.objects[1]);
  				auto obj2 = layout.get_object_array(pipeline::MemoryMapType::Read, allocated, object_layout.objects[2]);
  				auto obj3 = layout.get_object_array(pipeline::MemoryMapType::Read, allocated, object_layout.objects[3]);
  				auto obj4 = layout.get_object_array(pipeline::MemoryMapType::Read, allocated, object_layout.objects[4]);
          char *abs_offset = (char *)obj0.at_stream(0).get();
          for (std::size_t i = 0; i < stream_count; ++i)
          {
            REQUIRE(obj0.at_stream(i).get() == (void*)(abs_offset + 0));
            REQUIRE(obj1.at_stream(i).get() == (void*)(abs_offset + 8));
            REQUIRE(obj2.at_stream(i).get() == (void*)(abs_offset + 16));
            REQUIRE(obj3.at_stream(i).get() == (void*)(abs_offset + 24));

            REQUIRE(obj0.map_type == pipeline::MemoryMapType::Read);
            REQUIRE(obj0.offset == 0);
            REQUIRE(obj0.DummyMemory::MappedType::size == std::numeric_limits<std::size_t>::max());

            abs_offset += TestObjectLayout::expected_stream_size + TestObjectLayout::expected_stream_padding;
          }

          for (std::size_t i = 0; i < stream_count; ++i)
          {
            REQUIRE(obj4.at_stream(i).get() == (void*)(abs_offset));
          }
        }
      }

      THEN("constructors ae called correctly")
      {
        REQUIRE(LayoutTypeA::ctor_count == 9);
        REQUIRE(LayoutTypeA::copy_count == 0);
        REQUIRE(LayoutTypeA::dtor_count == 9);
      }

    }
  }
}

SCENARIO("SOAMemoryLayout", "[memory]")
{
  GIVEN("Test object layout")
  {
    static const std::size_t stream_count = 4;
    TestObjectLayout object_layout;
    pipeline::SOAMemoryLayout<DummyMemory> layout{ object_layout.objects, stream_count };

    WHEN("Creating memory from layout")
    {
      struct {
        DummyMemory allocate(const pipeline::MemoryDescription &desc)
        {
          allocations.push_back(desc);
          return DummyMemory{};
        }

        std::vector<pipeline::MemoryDescription> allocations;
      } test_allocator;

      {
        LayoutTypeA::reset();
        auto allocated = layout.create(test_allocator);

        THEN("constructors ae called correctly")
        {
          REQUIRE(LayoutTypeA::ctor_count == 9);
          REQUIRE(LayoutTypeA::copy_count == 0);
          REQUIRE(LayoutTypeA::dtor_count == 0);
        }

        THEN("Allocation routine is called correctly")
        {
          REQUIRE(test_allocator.allocations.size() == 5);

          REQUIRE(test_allocator.allocations[0].size() == sizeof(LayoutTypeA) * stream_count);
          REQUIRE(test_allocator.allocations[0].alignment() == std::alignment_of<LayoutTypeA>::value);

          REQUIRE(test_allocator.allocations[1].size() == sizeof(LayoutTypeB) * stream_count);
          REQUIRE(test_allocator.allocations[1].alignment() == std::alignment_of<LayoutTypeB>::value);

          REQUIRE(test_allocator.allocations[2].size() == sizeof(LayoutTypeA) * stream_count);
          REQUIRE(test_allocator.allocations[2].alignment() == std::alignment_of<LayoutTypeA>::value);

          REQUIRE(test_allocator.allocations[3].size() == sizeof(LayoutTypeB) * stream_count);
          REQUIRE(test_allocator.allocations[3].alignment() == std::alignment_of<LayoutTypeB>::value);

          REQUIRE(test_allocator.allocations[4].size() == sizeof(LayoutTypeA));
          REQUIRE(test_allocator.allocations[4].alignment() == std::alignment_of<LayoutTypeA>::value);
        }

        THEN("objects are accessed correctly")
        {
          for (auto &object : object_layout.objects)
          {
						auto obj = layout.get_object_array(pipeline::MemoryMapType::Read, allocated, object);
						auto start = (char *)obj.at_stream(0).get();
            for (std::size_t i = 0; i < stream_count; ++i)
            {
              REQUIRE(obj.map_type == pipeline::MemoryMapType::Read);
              REQUIRE(obj.offset == 0);
              REQUIRE(obj.DummyMemory::MappedType::size == std::numeric_limits<std::size_t>::max());

              if (!object.is_per_stream())
              {
                REQUIRE(obj.at_stream(i).get() == start);
              }
              else
              {
                auto location = i * object.type()->memory_description().size();
                REQUIRE(obj.at_stream(i).get() == start + location);
              }
            }
          }
        }
      }

      THEN("constructors ae called correctly")
      {
        REQUIRE(LayoutTypeA::ctor_count == 9);
        REQUIRE(LayoutTypeA::copy_count == 0);
        REQUIRE(LayoutTypeA::dtor_count == 9);
      }
    }
  }
}
