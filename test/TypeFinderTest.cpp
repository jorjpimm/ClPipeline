#include "pipeline/TypeFinder.h"

#include <catch/catch.hpp>

struct TestFinderType
{
};

PIPELINE_DEFINE_TYPE(TestFinderType)
PIPELINE_IMPLEMENT_TYPE(TestFinderType)

SCENARIO("TypeFinder", "[type]")
{
  THEN("A type find returns same objects")
  {
    auto type1 = pipeline::TypeFinder<TestFinderType>::get();
    auto type2 = pipeline::TypeFinder<TestFinderType>::get();

    REQUIRE(type1 == type2);
  }

  THEN("A type find produces correct object")
  {
    auto type = pipeline::TypeFinder<TestFinderType>::get();

    REQUIRE(type->name() == "TestFinderType");
    REQUIRE(type->memory_description() == pipeline::MemoryDescription::for_type<TestFinderType>());
  }
}
