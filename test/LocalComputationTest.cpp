#include "pipeline/Computation/LocalComputation.h"
#include "pipeline/TypedObject.h"
#include "pipeline/TypeFinder.h"

#include <catch/catch.hpp>

struct LocaLocalComputationCalled
{
  int called_count;
};

PIPELINE_DEFINE_TYPE(LocaLocalComputationCalled)
PIPELINE_IMPLEMENT_TYPE(LocaLocalComputationCalled)

SCENARIO("LocalComputation", "[computation]")
{
  GIVEN("a computation object")
  {
    auto arg_type = pipeline::TypeFinder<LocaLocalComputationCalled>::get();
    auto compute = [](const pipeline::LocalPerStreamComputation::Arguments &in_args)
    {
      auto called = in_args.at<LocaLocalComputationCalled>(0);
      ++called.get().called_count;
    };
    pipeline::LocalPerStreamComputation comp(
      { },
      { arg_type },
      compute
    );

    THEN("accessors are correct")
    {
      auto exp_inputs = std::vector<const pipeline::Type *>{ };
      auto exp_outputs = std::vector<pipeline::Computation::Output>{ arg_type };
      REQUIRE(comp.inputs() == exp_inputs);
      REQUIRE(comp.outputs() == exp_outputs);
      REQUIRE(comp.type() == pipeline::Computation::ComputationType::Local);
    }

    THEN("Invoking computation works")
    {
      LocaLocalComputationCalled called{};
      auto ref = pipeline::MemoryReference::initialise(&called, sizeof(called));
      auto object = pipeline::Object::initialise(ref, arg_type);
    
      auto objects = pipeline::ObjectArray::initialise(arg_type, ref, 1, 1, 0);
			std::array<pipeline::ObjectArray *, 1> objects_ptrs = { {&objects} };
      pipeline::LocalComputation::MultiStreamArguments all_args(objects_ptrs);
    
      REQUIRE(called.called_count == 0);
      comp.call(0, all_args);
      REQUIRE(called.called_count == 1);
      comp.call(0, all_args);
      REQUIRE(called.called_count == 2);
    }
  }
}
