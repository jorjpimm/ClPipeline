#pragma once

#if _MSC_VER
# define ALIGN_PREFIX(by) __declspec(align(by))
# define ALIGN_SUFFIX(by)
#else
# define ALIGN_PREFIX(by)
# define ALIGN_SUFFIX(by) __attribute__((__aligned__(by)));
#endif
