#include "pipeline/Memory/MemoryReference.h"
#include "utils.h"

#include <catch/catch.hpp>

struct ReferenceTest
{
  int a;
  float b;
};


ALIGN_PREFIX(32) struct AlignedReferenceTest
{
  int a;
  float b;
} ALIGN_SUFFIX(32);

SCENARIO("MemoryReference", "[memory]")
{
  GIVEN("a memory reference")
  {
    ReferenceTest obj;
    obj.a = 4;
    obj.b = 5.0f;

    auto ref = pipeline::MemoryReference::initialise(&obj, sizeof(obj));

    THEN("reference works correctly")
    {
      REQUIRE(ref.get() == &obj);

      auto a_ref = pipeline::MemoryReference::initialise_sub(
        ref,
        offsetof(ReferenceTest, a),
        sizeof(obj.a));

      REQUIRE(a_ref.get() == static_cast<char *>(ref.get()) + offsetof(ReferenceTest, a));

      auto b_ref = pipeline::MemoryReference::initialise_sub(
        ref,
        offsetof(ReferenceTest, b),
        sizeof(obj.b));

      REQUIRE(b_ref.get() == static_cast<char *>(ref.get()) + offsetof(ReferenceTest, b));
    }
  }

  GIVEN("an array memory reference")
  {
    const std::size_t COUNT = 8;
    AlignedReferenceTest obj[COUNT];
  
    auto el_desc = pipeline::MemoryDescription::for_type<AlignedReferenceTest>();

    auto arr = pipeline::MemoryReference::initialise(&obj, sizeof(obj));

    for (std::size_t i = 0; i < COUNT; ++i)
    {
      auto el = pipeline::MemoryReference::initialise_array_element(arr, el_desc, i);
      REQUIRE(el.get() == obj + i);
    }
  }

}
